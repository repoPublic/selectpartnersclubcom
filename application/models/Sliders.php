<?php 


Class Sliders extends CI_Model {


	public function get_edit(){
		$this->db->where('id',$this->id);
		$row = $this->db->get('Sliders')->result_array();
		return $row[0];
	}

	public function add(){
		$this->db->insert('Sliders',$this->data);
		return $this->db->insert_id();
	}

	public function update(){
		$this->db->where('id',$this->id);
		$this->db->update('Sliders',$this->data);
		return $this->db->affected_rows();
	}

	public function remove(){
		$this->db->where('id',$this->id);
		$this->db->delete('Sliders');
		return $this->db->affected_rows();
	}

	
	public function get_list(){
		$this->db->order_by("sort", "asc");
		$data = $this->db->get('Sliders')->result_array();
		return $data;
	}


}


?>