<?php 


Class Videos extends CI_Model {
	public $id;

	public function get_list(){
        $this->db->order_by("sort", "asc");
		return $this->db->get('Videos')->result_array();
	}

	public function get_edit(){
		$this->db->where('id',$this->id);
		$row = $this->db->get('Videos')->result_array();
		return $row[0];
	}

	public function add(){
		$this->db->insert('Videos',$this->data);
		return $this->db->insert_id();
	}

	public function update(){
		$this->db->where('id',$this->id);
		$this->db->update('Videos',$this->data);
		return $this->db->affected_rows();
	}

	public function remove(){
		$this->db->where('id',$this->id);
		$this->db->delete('Videos');
		return $this->db->affected_rows();
	}

	


}


?>