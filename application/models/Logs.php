<?php 

class Logs extends CI_Model {
	public $id;

	public function get_insert(){
        $this->db->insert('Logs',$this);
        return $this->db->insert_id();
    }

    public function get_list(){
    	$query = $this->db->query("SELECT * FROM Logs INNER JOIN User ON Logs.user_id = User.id");
    	return $query->result_array();
    }

    public function get_all(){
    	$log = $this->input->post(null,true);
    	
    	$query = $this->db->query("
									SELECT
										Logs.id,
										User.name,
										@Pname:=(SELECT Partner.p_name FROM Partner WHERE Partner.p_id = User.partner_id) as Pname,
										Logs.created_date, 
										Logs.comment, 
										Logs.ip_address
									FROM Logs 
									INNER JOIN User ON Logs.user_id = User.id 
									INNER JOIN Partner ON Partner.p_id = User.partner_id 
									WHERE 
										Logs.created_date >= '".$log['startDate']." 00:00:00' AND 
										Logs.created_date <= '".$log['endDate']." 23:59:59' AND 
										Partner.p_name = '{$log['partner']}' AND 
										User.name = '{$log['user']}'
									ORDER BY Logs.created_date DESC
								");
    	return $query->result_array();
    }

    public function get_all_excel(){
    	$log = $this->input->post(null,true);
    	
    	$query = $this->db->query("
									SELECT
										User.name,
										@Pname:=(SELECT Partner.p_name FROM Partner WHERE Partner.p_id = User.partner_id) as Pname,
										Logs.created_date, 
										Logs.comment, 
										Logs.ip_address
									FROM Logs 
									INNER JOIN User ON Logs.user_id = User.id 
									INNER JOIN Partner ON Partner.p_id = User.partner_id 
									WHERE 
										Logs.created_date >= '".$log['startDate']." 00:00:00' AND 
										Logs.created_date <= '".$log['endDate']." 23:59:59' AND 
										Partner.p_name = '{$log['partner']}' AND 
										User.name = '{$log['user']}'
									ORDER BY Logs.created_date DESC
								");
    	return $query;
    }

    public function get_only_date(){
    	$log = $this->input->post(null,true);

    	$query = $this->db->query("
									SELECT
										Logs.id,
										User.name,
										@Pname:=(SELECT Partner.p_name FROM Partner WHERE Partner.p_id = User.partner_id) as Pname,
										Logs.created_date, 
										Logs.comment, 
										Logs.ip_address
									FROM Logs 
									INNER JOIN User ON Logs.user_id = User.id 
									INNER JOIN Partner ON Partner.p_id = User.partner_id 
									WHERE 
										Logs.created_date >= '".$log['startDate']." 00:00:00' AND 
										Logs.created_date <= '".$log['endDate']." 23:59:59'
									ORDER BY Logs.created_date DESC
								");
    	return $query->result_array();
    }

    public function get_only_date_excel(){
    	$log = $this->input->post(null,true);

    	$query = $this->db->query("
									SELECT
										User.name,
										@Pname:=(SELECT Partner.p_name FROM Partner WHERE Partner.p_id = User.partner_id) as Pname,
										Logs.created_date, 
										Logs.comment, 
										Logs.ip_address
									FROM Logs 
									INNER JOIN User ON Logs.user_id = User.id 
									INNER JOIN Partner ON Partner.p_id = User.partner_id 
									WHERE 
										Logs.created_date >= '".$log['startDate']." 00:00:00' AND 
										Logs.created_date <= '".$log['endDate']." 23:59:59'
									ORDER BY Logs.created_date DESC
								");
    	return $query;
    }

    public function get_only_user(){
    	$log = $this->input->post(null,true);
    	
    	$query = $this->db->query("
									SELECT
										Logs.id,
										User.name,
										@Pname:=(SELECT Partner.p_name FROM Partner WHERE Partner.p_id = User.partner_id) as Pname,
										Logs.created_date, 
										Logs.comment, 
										Logs.ip_address
									FROM Logs 
									INNER JOIN User ON Logs.user_id = User.id 
									INNER JOIN Partner ON Partner.p_id = User.partner_id 
									WHERE 
										Logs.created_date >= '".$log['startDate']." 00:00:00' AND 
										Logs.created_date <= '".$log['endDate']." 23:59:59' AND 
										User.name = '{$log['user']}'
									ORDER BY Logs.created_date DESC
								");
    	return $query->result_array();
    }

    public function get_only_user_excel(){
    	$log = $this->input->post(null,true);
    	
    	$query = $this->db->query("
									SELECT
										User.name,
										@Pname:=(SELECT Partner.p_name FROM Partner WHERE Partner.p_id = User.partner_id) as Pname,
										Logs.created_date, 
										Logs.comment, 
										Logs.ip_address
									FROM Logs 
									INNER JOIN User ON Logs.user_id = User.id 
									INNER JOIN Partner ON Partner.p_id = User.partner_id 
									WHERE 
										Logs.created_date >= '".$log['startDate']." 00:00:00' AND 
										Logs.created_date <= '".$log['endDate']." 23:59:59' AND 
										User.name = '{$log['user']}'
									ORDER BY Logs.created_date DESC
								");
    	return $query;
    }

    public function get_only_partner(){
    	$log = $this->input->post(null,true);
    	
    	$query = $this->db->query("
									SELECT
										Logs.id,
										User.name,
										@Pname:=(SELECT Partner.p_name FROM Partner WHERE Partner.p_id = User.partner_id) as Pname,
										Logs.created_date, 
										Logs.comment, 
										Logs.ip_address
									FROM Logs 
									INNER JOIN User ON Logs.user_id = User.id 
									INNER JOIN Partner ON Partner.p_id = User.partner_id 
									WHERE 
										Logs.created_date >= '".$log['startDate']." 00:00:00' AND 
										Logs.created_date <= '".$log['endDate']." 23:59:59' AND 
										Partner.p_name = '{$log['partner']}'
									ORDER BY Logs.created_date DESC
								");
    	return $query->result_array();
    }

    public function get_only_partner_excel(){
    	$log = $this->input->post(null,true);
    	
    	$query = $this->db->query("
									SELECT
										User.name,
										@Pname:=(SELECT Partner.p_name FROM Partner WHERE Partner.p_id = User.partner_id) as Pname,
										Logs.created_date, 
										Logs.comment, 
										Logs.ip_address
									FROM Logs 
									INNER JOIN User ON Logs.user_id = User.id 
									INNER JOIN Partner ON Partner.p_id = User.partner_id 
									WHERE 
										Logs.created_date >= '".$log['startDate']." 00:00:00' AND 
										Logs.created_date <= '".$log['endDate']." 23:59:59' AND 
										Partner.p_name = '{$log['partner']}'
									ORDER BY Logs.created_date DESC
								");
    	return $query;
    }

    public function get_most_clicked_pages(){
    	$log = $this->input->post(null,true);

    	$query = $this->db->query("
								SELECT
									Logs.comment, 
									Count(Logs.comment) as comment_total
								FROM Logs
								WHERE 
									Logs.created_date >= '".$log['startDate']." 00:00:00' AND 
									Logs.created_date <= '".$log['endDate']." 23:59:59'
								GROUP BY Logs.comment
								ORDER BY comment_total DESC
							");

    	return $query;
    }


}

?>