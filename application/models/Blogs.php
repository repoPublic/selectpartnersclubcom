<?php 

class Blogs extends CI_Model {
	public $id;
	public $title;

	public function get_list(){
		$this->db->order_by("id", "desc");
		return $this->db->get('Blogs')->result_array();
	}

	public function get_list_5(){
		$this->db->order_by("id", "desc");
		return $this->db->get('Blogs',5)->result_array();
	}

	public function get_detail($id){
		$this->db->where('id',$id);
		$data = $this->db->get('Blogs')->result_array();
		return $data[0];
	}

	public function get_search($title){
		$query = $this->db->query("SELECT * FROM Blogs WHERE title LIKE '%{$title}%' LIMIT 5"); 
		return $query->result_array();
	}



}

?>