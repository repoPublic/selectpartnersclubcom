<?php 

class My_Gift extends CI_Model {
	public $id;

	public function get_list(){
		$query = $this->db->query("
            SELECT Gifts.name as gName, DATE_FORMAT(My_Gift.created_date, '%d/%m/%Y') as mygDate, My_Gift.active as gActive, gift_id, Gifts.point as gPoint
            FROM My_Gift 
            LEFT JOIN Gifts ON My_Gift.gift_id = Gifts.id 
            LEFT JOIN Partner ON My_Gift.partner_id = Partner.p_id
            WHERE My_Gift.partner_id = '{$_SESSION["partner_id"]}' 
        ");

        return $query->result_array();
	}

    public function get_list1($id1){
        $query = $this->db->query("
            SELECT Gifts.name as gName, DATE_FORMAT(My_Gift.created_date, '%d/%m/%Y') as mygDate, My_Gift.active as gActive, gift_id, Gifts.point as gPoint
            FROM My_Gift 
            LEFT JOIN Gifts ON My_Gift.gift_id = Gifts.id 
            LEFT JOIN Partner ON My_Gift.partner_id = Partner.p_id
            WHERE My_Gift.partner_id = '{$id1}' 
        ");

        return $query->result_array();
    }

    public function get_edit(){
        $this->db->where('id',$this->id);
        $row = $this->db->get('My_Gift')->result_array();
        return $row[0];
    }

    public function add(){
        $this->db->insert('My_Gift',$this->data);
        return $this->db->insert_id();
    }

    public function update(){
        $this->db->where('id',$this->id);
        $this->db->update('My_Gift',$this->data);
        return $this->db->affected_rows();
    }

    public function remove(){
        $this->db->where('id',$this->id);
        $this->db->delete('My_Gift');
        return $this->db->affected_rows();
    }


}

?>