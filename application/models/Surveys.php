<?php 

class Surveys extends CI_Model {
	public $id;


    public function get_edit(){
        $this->db->where('id',$this->id);
        $row = $this->db->get('Surveys')->result_array();
        return $row[0];
    }

    public function add(){
        $this->db->insert('Surveys',$this->data);
        return $this->db->insert_id();
    }

    public function update(){
        $this->db->where('id',$this->id);
        $this->db->update('Surveys',$this->data);
        return $this->db->affected_rows();
    }

    public function remove(){
        $this->db->where('id',$this->id);
        $this->db->delete('Surveys');
        return $this->db->affected_rows();
    }


}

?>