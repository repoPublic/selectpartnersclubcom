<?php 

class Movements extends CI_Model {
	public $id;

	public function get_list(){
		$query = $this->db->query("
            SELECT *
            FROM Movements 
            LEFT JOIN Partner ON Movements.partner_id = Partner.p_id
            WHERE Partner.p_id = '{$_SESSION["partner_id"]}' 
        ");

        return $query->result_array();
	}

    public function get_list1($id1){
        $query = $this->db->query("
            SELECT *
            FROM Movements 
            LEFT JOIN Partner ON Movements.partner_id = Partner.p_id
            WHERE Partner.p_id = '{$id1}'
        ");

        return $query->result_array();
    }

    public function get_edit(){
        $this->db->where('id',$this->id);
        $row = $this->db->get('Movements')->result_array();
        return $row[0];
    }

    public function add(){
        $this->db->insert('Movements',$this->data);
        return $this->db->insert_id();
    }

    public function update(){
        $this->db->where('id',$this->id);
        $this->db->update('Movements',$this->data);
        return $this->db->affected_rows();
    }

    public function remove(){
        $this->db->where('id',$this->id);
        $this->db->delete('Movements');
        return $this->db->affected_rows();
    }


}

?>