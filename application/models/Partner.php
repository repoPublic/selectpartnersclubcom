<?php 

class Partner extends CI_Model {
	public $p_id;

	public function get_list(){
		return $this->db->get('Partner')->result_array();
	}

    public function get_edit(){
        $this->db->where('p_id',$this->p_id);
        $row = $this->db->get('Partner')->result_array();
        return $row[0];
    }

    public function add(){
        $this->db->insert('Partner',$this->data);
        return $this->db->insert_id();
    }

    public function update(){
        $this->db->where('p_id',$this->p_id);
        $this->db->update('Partner',$this->data);
        return $this->db->affected_rows();
    }

    public function remove(){
        $this->db->where('p_id',$this->p_id);
        $this->db->delete('Partner');
        return $this->db->affected_rows();
    }


}

?>