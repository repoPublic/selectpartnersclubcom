<?php 

class User extends CI_Model {
	public $id;

	public function get_list($id){
		$query = $this->db->query("SELECT * FROM User WHERE id = '{$id}' ");
        return $query->row_array();
	}

    public function get_list_user(){
        return $this->db->get('User')->result_array();
    }

	public function get_list_join_partner($id){
		$query = $this->db->query("SELECT * FROM User INNER JOIN Partner ON User.partner_id = Partner.p_id WHERE id = '{$id}' ");
        return $query->row_array();
	}

	public function get_invoice_detail(){
		$query = $this->db->query("
            SELECT inv.no as Ino, inv.created as Icreated, pro.brand, pro.model, pro.no as Pno, pro.name as Pname, pro.points_multiplier, pro.point as Ppoint, pro.product_category_id as pcid, inD.piece
            FROM Invoice_Detail inD 
            LEFT JOIN Products pro ON inD.product_no = pro.no 
            LEFT JOIN Invoices inv ON inD.invoice_no = inv.no 
            LEFT JOIN Partner ON inv.vergi_no = Partner.p_tax_number
            WHERE Partner.p_id = '{$_SESSION["partner_id"]}' 
        ");

        return $query->result_array();
	}

    public function get_invoice_detail1($id1){
        $query = $this->db->query("
            SELECT inv.no as Ino, inv.created as Icreated, pro.brand, pro.model, pro.no as Pno, pro.name as Pname, pro.points_multiplier, pro.point as Ppoint, pro.product_category_id as pcid, inD.piece
            FROM Invoice_Detail inD 
            LEFT JOIN Products pro ON inD.product_no = pro.no
            LEFT JOIN Invoices inv ON inD.invoice_no = inv.no
            LEFT JOIN Partner ON inv.vergi_no = Partner.p_tax_number
            WHERE Partner.p_id = '{$id1}'
        ");

        return $query->result_array();
    }

	public function get_update(){
		$this->db->where('id',$this->id);
		$this->db->update('User',$this);
		return $this->db->affected_rows();
	}

	public function get_edit(){
        $this->db->where('id',$this->id);
        $row = $this->db->get('User')->result_array();
        return $row[0];
    }

    public function add(){
        $this->db->insert('User',$this->data);
        return $this->db->insert_id();
    }

    public function update(){
        $this->db->where('id',$this->id);
        $this->db->update('User',$this->data);
        return $this->db->affected_rows();
    }

    public function remove(){
        $this->db->where('id',$this->id);
        $this->db->delete('User');
        return $this->db->affected_rows();
    }


}

?>