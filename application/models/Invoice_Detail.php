<?php 


Class Invoice_Detail extends CI_Model {


	public function get_edit(){
		$this->db->where('id',$this->id);
		$row = $this->db->get('Invoice_Detail')->result_array();
		return $row[0];
	}

	public function add(){
		$this->db->insert('Invoice_Detail',$this->data);
		return $this->db->insert_id();
	}

	public function update(){
		$this->db->where('id',$this->id);
		$this->db->update('Invoice_Detail',$this->data);
		return $this->db->affected_rows();
	}

	public function remove(){
		$this->db->where('id',$this->id);
		$this->db->delete('Invoice_Detail');
		return $this->db->affected_rows();
	}

	
	public function get_list(){
		$data = $this->db->get('Invoice_Detail')->result_array();
		return $data;
	}


}


?>