<?php 

class Questions extends CI_Model {
	public $id;


    public function get_edit(){
        // $this->db->where('id',$this->id);
        // $row = $this->db->get('Questions')->result_array();

        $row = $this->db->query("
                        SELECT *
                        FROM Questions 
                        INNER JOIN Survey_Question_relationship ON Questions.id = Survey_Question_relationship.question_id 
                        WHERE Questions.id = '{$this->id}'
        ")->result_array();

        return $row[0];
    }

    public function add(){
        $this->db->insert('Questions',$this->data);
        return $this->db->insert_id();
    }

    public function update(){
        $this->db->where('id',$this->id);
        $this->db->update('Questions',$this->data);
        return $this->db->affected_rows();
    }

    public function remove(){
        $this->db->where('id',$this->id);
        $this->db->delete('Questions');
        return $this->db->affected_rows();
    }


}

?>