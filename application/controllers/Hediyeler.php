<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hediyeler extends CI_Controller {

    function __construct() {
        parent::__construct();
        if(!$_SESSION['id']){
            redirect(base_url().'login');
        }
    }

    public function index()
    {
        $this->load->model('User');
        $this->load->model('My_Gift');
        $this->load->model('Gifts');
        $this->load->model('Movements');

        $this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Hediyeler sayfasına girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $data['gifts'] = $this->My_Gift->get_list($this->session->partner_id);
        $data['sales'] = $this->User->get_invoice_detail();
        $data['gift_all'] = $this->Gifts->get_list(); 
        $data['movements'] = $this->Movements->get_list($this->session->partner_id);
        $data['campaigns'] = $this->db->query("SELECT * FROM Campaigns WHERE partner_id = '{$_SESSION["partner_id"]}' ")->result_array();

        $campaigns_point = 0;
        foreach ($data['campaigns'] as $cmp) {
                $campaigns_point = $cmp['score'] + $campaigns_point;
        }

        $movement=0;
        foreach ($data['movements'] as $mv) {
            // if ($mv['active'] == 1) {
                $movement = $mv['score'] + $movement;
            // }
        }

        $gift = 0;
        foreach ($data['gifts'] as $g) {
            if ($g['gActive'] == 1) {
                $gift = $g['gPoint'] + $gift;
            }
        }

        $primergy_toplam = 0;
        $eternus_toplam = 0;

        foreach ($data['sales'] as $sale) {
            if ($sale['pcid'] == 1) {
                $primergy_toplam = ($sale['Ppoint'] * $sale['piece']) + $primergy_toplam;
            }
            if ($sale['pcid'] == 2) {
                $eternus_toplam = ($sale['Ppoint'] * $sale['piece']) + $eternus_toplam;
            }
        }

        $data['primergy_toplam'] = $primergy_toplam;
        $data['eternus_toplam'] = $eternus_toplam;
        $data['movement_point'] = $movement;
        $data['kampanya_puan'] = $campaigns_point;
        $data['kullanilan_puan'] = $gift;

        $data['toplam'] = ($primergy_toplam + $eternus_toplam + $movement + $campaigns_point) - $gift;

        $this->load->view('hediyeler', $data);
    }

    public function add(){
        $this->load->model('Logs');

    	$hediyeler = $this->input->post();
    	$mysql_date_now = date("Y-m-d H:i:s", strtotime('+3 hours'));

        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s");
        $this->Logs->comment = "Hediyeler sayfasında '" . $hediyeler['hediye'] . "' isimli hediye almak için istek gönderildi.";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

    	$query = $this->db->query("
    		INSERT INTO My_Gift (user_id, partner_id, gift_id, created_date, active) 
    		VALUES ('{$_SESSION["id"]}', '{$_SESSION["partner_id"]}', '{$hediyeler['hediye']}', '{$mysql_date_now}', 0)
    	"); 

        if (isset($_POST['profil'])) {
            redirect(base_url().'profil/satislarim#basarili');
        }else{
            redirect(base_url().'hediyeler#tebrikler');
        }
    }

}
