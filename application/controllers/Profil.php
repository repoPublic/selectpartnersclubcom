<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

    function __construct() {
        parent::__construct();
        if(!$_SESSION['id']){
            redirect(base_url().'login');
        }
    }

    public function index()
    {
        $this->load->model('User');
        $this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Profil sayfasına girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $data['profil'] = $this->User->get_list_join_partner($this->session->id);
        $this->load->view('profil', $data);
    }

    public function satislarim()
    {   
        $this->load->model('User');
        $this->load->model('My_Gift');
        $this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Profil sayfasının içindeki satışlarım sayfasına girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $data['sales'] = $this->User->get_invoice_detail();
        $data['profil'] = $this->User->get_list_join_partner($this->session->id);
        $data['gifts'] = $this->My_Gift->get_list($this->session->partner_id);
        $data['sales'] = $this->User->get_invoice_detail();

        $gift = 0;
        foreach ($data['gifts'] as $g) {
            if ($g['gActive'] == 1) {
                if ($g['gift_id'] == 14) {
                    $gift = $g['gPoint'] + $gift;
                }
            }
        }

        $eternus_toplam = 0;
        foreach ($data['sales'] as $sale) {
            if ($sale['pcid'] == 2) {
                $eternus_toplam = ($sale['Ppoint'] * $sale['piece']) + $eternus_toplam;
            }
        }

        // $data['eternus_toplam'] = $eternus_toplam - $gift;
        $data['eternus_toplam'] = $eternus_toplam - $gift;

        $this->load->view('profil-satislarim', $data);
    }

    public function puanekstresi()
    {
        $this->load->model('User');
        $this->load->model('My_Gift');
        $this->load->model('Movements');
        $this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Profil sayfasının içindeki puan ekstresi sayfasına girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }
        
        $data['sales'] = $this->User->get_invoice_detail();
        $data['profil'] = $this->User->get_list_join_partner($this->session->id);
        $data['gifts'] = $this->My_Gift->get_list($this->session->partner_id);
        $data['movements'] = $this->Movements->get_list($this->session->partner_id);
        $data['campaigns'] = $this->db->query("SELECT * FROM Campaigns WHERE partner_id = '{$_SESSION["partner_id"]}' ")->result_array();

        $movement=0;
        foreach ($data['movements'] as $mv) {
            // if ($mv['active'] == 1) {
                $movement = $mv['score'] + $movement;
            // }
        }

        $gift = 0;
        foreach ($data['gifts'] as $g) {
            if ($g['gActive'] == 1) {
                $gift = $g['gPoint'] + $gift;
            }
        }

        $primergy_toplam = 0;
        $eternus_toplam = 0;

        foreach ($data['sales'] as $sale) {
            if ($sale['pcid'] == 1) {
                $primergy_toplam = ($sale['Ppoint'] * $sale['piece']) + $primergy_toplam;
            }
            if ($sale['pcid'] == 2) {
                $eternus_toplam = ($sale['Ppoint'] * $sale['piece']) + $eternus_toplam;
            }
        }

        $campaigns_point = 0;
        foreach ($data['campaigns'] as $cmp) {
                $campaigns_point = $cmp['score'] + $campaigns_point;
                if ($cmp['name'] = "3 Adet PRIMERGY RX540 Server ve Eternus DX100 Storage") {
                    $primergy_toplam = $primergy_toplam - 1440; 
                    $eternus_toplam = $eternus_toplam - 1200; 
                }
                elseif($cmp['name'] = "2 Adet PRIMERGY RX540 Server ve Eternus DX100 Storage"){
                    $primergy_toplam = $primergy_toplam - 960; 
                    $eternus_toplam = $eternus_toplam - 1200; 
                }
                elseif($cmp['name'] = "3 Adet PRIMERGY RX510 Server ve Eternus DX100 Storage"){
                    $primergy_toplam = $primergy_toplam - 960; 
                    $eternus_toplam = $eternus_toplam - 1200; 
                }

        }

        $data['primergy_toplam'] = $primergy_toplam;
        $data['eternus_toplam'] = $eternus_toplam;
        $data['movement_point'] = $movement;
        $data['kampanya_puan'] = $campaigns_point;
        $data['kullanilan_puan'] = $gift;

        $data['toplam'] = ($primergy_toplam + $eternus_toplam + $movement + $campaigns_point) - $gift;

        $this->load->view('profil-puanekstresi', $data);
    }

    public function hediyelerim()
    {
        $this->load->model('User');
        $this->load->model('My_Gift');
        $this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Profil sayfasının içindeki hediyelerim sayfasına girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $data['profil'] = $this->User->get_list_join_partner($this->session->id);
        $data['gifts'] = $this->My_Gift->get_list($this->session->id);

        $this->load->view('profil-hediyelerim', $data);
    }

    public function profil_update($id){

        $this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Profil bilgileri güncellendi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $query = $this->db->query("UPDATE User u
                                        JOIN Partner p ON u.partner_id = p.p_id SET 
                                        p.p_tax_office = '{$_POST['Vergi_Dairesi']}',
                                        p.p_tax_number = '{$_POST['Vergi_Numarasi']}',
                                        p.p_address = '{$_POST['Firma_Adresi']}',
                                        p.p_tel = '{$_POST['Firma_Telefonu']}',
                                        p.p_email = '{$_POST['Firma_E_posta']}',
                                        u.position = '{$_POST['Firmadaki_Gorevi']}',
                                        u.email = '{$_POST['E_posta']}',
                                        u.gsm = '{$_POST['Kullanici_Telefonu']}'
                                        WHERE u.id = '{$_SESSION["id"]}' ");

        redirect(base_url().'profil');
    }

    public function upload_picture($id){
        $this->load->model('User');  
        $this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Profil fotoğrafı güncellendi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }
           

        $this->User->id = $id;

        $config['upload_path']   = $_SERVER['DOCUMENT_ROOT'] . '/uploaded_files/';
        $config['allowed_types'] = 'gif|jpg|png'; 
         // $config['max_size']      = 100000; 

        $this->load->library('upload', $config);

            
         if ( ! $this->upload->do_upload('picture')) {
            $error = array('error' => $this->upload->display_errors()); 
            print_r($error);
            // $this->load->view('upload_form', $error); 
         }
            
         else { 
            $config2['image_library'] = 'gd2';
            $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
            $config2['new_image'] = $_SERVER['DOCUMENT_ROOT'] . '/uploaded_files/';
            $config2['maintain_ratio'] = TRUE;
            $config2['create_thumb'] = FALSE;
            // $config2['thumb_marker'] = '_thumb';
            $config2['width'] = 121;
            $config2['height'] = 121;
            $this->load->library('image_lib',$config2);
            $this->image_lib->resize();
         } 

        if ($_FILES['picture']['size'] > 0) {
            $this->User->picture = $_FILES['picture']['name'];
        }
       
        $this->User->get_update();

        redirect(base_url().'profil');
    }






















}