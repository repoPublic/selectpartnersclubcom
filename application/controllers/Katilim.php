<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katilim extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index(){	

        $query = $this->db->query("
        	SELECT p_id, p_name
        	FROM Partner
        ");

        $data['partners'] = $query->result_array();
        $this->load->view('katilim',$data);
    }



    public function add(){
    	$users = $this->input->post();

    	$query = $this->db->query("
    		INSERT INTO User (name, email, gsm, partner_id, position, active) 
    		VALUES ('{$users['Ad_Soyad']}', '{$users['E_posta']}', '{$users['Kullanici_Telefonu']}', '{$users['Firma']}', '{$users['position']}', 0)
    	"); 

		redirect(base_url().'katilim');
    }


}
