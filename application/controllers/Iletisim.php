<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iletisim extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Logs');
    }

    public function index()
    {   
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "İletişim sayfasına girildi.";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $this->load->view('bizeulasin');
    }

    public function contact_add(){

        $this->load->model('Contacts');

        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "İletişim formu kayıt edildi.";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $this->Contacts->adsoyad = $this->input->post('adsoyad');
        $this->Contacts->eposta = $this->input->post('eposta');
        $this->Contacts->tel = $this->input->post('tel');
        $this->Contacts->mesaj = $this->input->post('mesaj');
        if ($_FILES['upload_ek']['size'] > 0) {
            $dosya = upload('upload_ek');
            $this->Contacts->uploaded = $dosya;
        }

/*
        mail.yandex
        info@salectpartnersclub.com
        Info123*
        olarak duzenlenecek
        */


        $this->Contacts->get_insert();

        $html_start = '<html><head><meta charset="utf-8" /></head><body><table cellpadding="0" cellspacing="0" border="0"><tr><td>Ad Soyad</td><td>:</td><td>'.$_POST['adsoyad'].'</td></tr><tr><td>Telefon</td><td>:</td><td>'.$_POST['tel'].'</td></tr><tr><td>E-Mail</td><td>:</td><td>'.$_POST['eposta'].'</td></tr><tr><td>Mesaj</td><td>:</td><td>'.$_POST['mesaj'].'</td></tr>';
        
        if ($dosya) {
            $html_ek = '<tr><td>Dosya</td><td>:</td>
                            <td>
                                <a href="http://selectpartnersclub.com/uploaded_files/'.$dosya.'"> Dosyayı görüntülemek için tıklayınız</a></td>
                        </tr>';
        }
        
        $html_finish = '</table></body></html>';

        $body = $html_start.$html_ek.$html_finish;

        $postData = array(
        'from_email'=>'GOKSAN.KADAYIFCI.external@ts.fujitsu.com',
        'from_name'=>'Select Partners Club',
                   'to' => "GOKSAN.KADAYIFCI.external@ts.fujitsu.com",
                   'subject' => 'Select Partners Club İletişim Form',
                   'content' => $body
               );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://www.ballsurance.com:3111/send");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        redirect(base_url().'iletisim#success');

    }




}
