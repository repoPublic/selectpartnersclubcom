<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Blogs');
    }

    public function index(){
        $this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Blog sayfasına girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

    	$data['blogs'] = $this->Blogs->get_list();
    	$data['blog_5'] = $this->Blogs->get_list_5();

        $this->load->view('blog',$data);
    }

    public function detail($id){
        $this->load->model('Logs');

    	$data['blog_detail'] = $this->Blogs->get_detail($id);
    	$data['blog_5'] = $this->Blogs->get_list_5();

        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "'" . $data['blog_detail']['title'] . "'" . " başlıklı blog sayfasına girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

    	$this->load->view('blog_detail',$data);
    }

    public function search(){
        $this->load->model('Logs');
           
        $data['blog_5'] = $this->Blogs->get_list_5();

        $title = $this->input->post('search_text');

        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Blog sayfasında '". $title ."' başlığında arama yapıldı";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $data['blog_search'] = $this->Blogs->get_search($this->input->post('search_text'));
        // print_r($data['blog_search']);

        $this->load->view('blog_search', $data);
    }

}
