<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branding extends CI_Controller {

    function __construct() {
        parent::__construct();
        if(!$_SESSION['id']){
            redirect(base_url().'login');
        }
    }

    public function index()
    {   
        $this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment =  "Pazarlama Dökümanları sayfasına girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $this->load->view('branding');
    }
}
