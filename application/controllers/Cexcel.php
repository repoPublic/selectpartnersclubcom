<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cexcel extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User');
        // $this->load->library('export_excel');
    }

  // public function dExcel()
  //   {   
  //       $result = $this->User->get_list_export();

  //       $this->to_excel($result, 'lista_de_personas');


  //   }

    public function to_excel() {

            $result = $this->User->get_list_export();

            header('Content-Disposition: attachment; filename=data.xls');
            header('Content-type: application/force-download');
            header('Content-Transfer-Encoding: binary');
            header('Pragma: public');
            print "\xEF\xBB\xBF"; // UTF-8 BOM
            $h = array();
            foreach($result->result_array() as $row){
                foreach($row as $key=>$val){
                    if(!in_array($key, $h)){
                        $h[] = $key;   
                    }
                }
            }

            echo '<table><tr>';
            foreach($h as $key) {
                $key = ucwords($key);
                echo '<th>'.$key.'</th>';
            }
            echo '</tr>';

            foreach($result->result_array() as $row){
                echo '<tr>';
                foreach($row as $val){
                    echo '<td>'.$val.'</td>';    
                }
            }
            echo '</tr>';
            echo '</table>';


    }

    // function writeRow($val) {
    //     echo '<td>'.$val.'</td>'; 
    // }

    




}
