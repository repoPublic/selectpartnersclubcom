<?php 

Class Slider_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
		$this->load->view('manage/slider');
	}

	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select('id,img,title,sort',false);
		$this->datatables->from("Sliders");
		$this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" id="remove_$1" onclick="confirmation()">x</a>', 'id');


		$list = $this->datatables->generate();
		$js =  json_decode($list);
		$a = array();
		foreach($js->aaData as $k=>$v){
			$v[1] = ($v[1] != '') ?  '<img src="'.base_url().'uploaded_files/'.$v[1].'" width="50" />' : '';
			$a[]=$v;
		}

		$js->aaData = $a;
		echo json_encode($js);
	}

	public function add(){
		$post = $this->input->post(null,true);
		$this->load->model('Sliders');

		$this->Sliders->data = array(
									'title'=>$post['data']['title'],
									'img'=>$post['data']['img'],
									'img_mobile'=>$post['data']['img_mobile'],
									'link'=>$post['data']['link'],
									'sort'=>$post['data']['sort']
									);
		$content_id = $this->Sliders->add();
		
		echo $content_id;
	}

	public function updates(){
		$post = $this->input->post(null,true);
		$this->load->model('Sliders');
		$this->Sliders->id = $post['data']['id'];
		$this->Sliders->data = array(
									'title'=>$post['data']['title'],
									'img'=>$post['data']['img'],
									'img_mobile'=>$post['data']['img_mobile'],
									'link'=>$post['data']['link'],
									'sort'=>$post['data']['sort']
									);

		$up = $this->Sliders->update();
		
		echo $up;

		// echo $update;
	}

	public function remove(){
		$post = $this->input->post(null,true);
		$this->load->model('Sliders');
		$this->Sliders->id = $post['id'];
		echo $this->Sliders->remove();
	}

	public function edit($id){
		$this->load->model('Sliders');
		$this->Sliders->id = $id;
		$data = $this->Sliders->get_edit();	

		echo json_encode($data);
	}




}


?>