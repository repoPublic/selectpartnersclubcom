<?php 

Class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index(){
		$this->load->view('manage/login');
	}

	public function enter(){
		$post = $this->input->post(null,true);
		$this->load->model('Admin');
		$this->Admin->email = $post['email'];
		$this->Admin->password = $post['password'];
		$user = $this->Admin->validate();
		if($user){
			$this->session->set_userdata('admin',$user[0]);
			echo 1;
		}else{
			echo 0;
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url().'manage/login');
	}
}

?>