<?php 

Class contact_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
		$this->load->view('manage/contact');
	}

	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select("id,uploaded,adsoyad,eposta,tel,mesaj");
		$this->datatables->from("Contacts");
		// $this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" id="remove_$1" onclick="confirmation()">x</a>', 'id');

		$list = $this->datatables->generate();
		$js =  json_decode($list);
		$a = array();
		foreach($js->aaData as $k=>$v){
			$v[1] = ($v[1] != '') ?  '<a href="'.base_url().'uploaded_files/'.$v[1].'" target="_blank">(ÖNİZLE)</a>&nbsp;&nbsp;<a href="'.base_url().'uploaded_files/'.$v[1].'" download >(İNDİR)</a>' : '';
			$a[]=$v;
		}

		$js->aaData = $a;
		echo json_encode($js);
	}


	public function remove(){
		$post = $this->input->post(null,true);
		$this->load->model('Contacts');
		$this->Contacts->id = $post['id'];
		echo $this->Contacts->remove();
	}



}


?>