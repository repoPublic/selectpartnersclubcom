<?php 

Class Report_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){

		$this->load->view('manage/report');
	}

	public function date_get(){
		$this->load->model('Logs');
		$log = $this->input->post(null,true);

		$data['userAndPartner'] = $this->db->query("SELECT * FROM User INNER JOIN Partner ON Partner.p_id = User.partner_id")->result_array();


		if($log['startDate'] != "" AND $log['endDate'] != ""){



			if (!$log['user'] AND !$log['partner']) {

				// $data['reports'] = $this->Logs->get_only_date();
				$control = 1;
				$this->to_excel('get_only_date_excel','data');

			}else if( $log['user'] AND $log['partner'] ){

				foreach ($data['userAndPartner'] as $UaP) {

					if( $log['user'] != $UaP['name'] AND $log['partner'] == $UaP['p_name']){

						$control = "";
						
					}
				}

				foreach ($data['userAndPartner'] as $UaP) {

					if( $log['user'] == $UaP['name'] AND $log['partner'] != $UaP['p_name']){
						
						$control = "";

					}

				}

				foreach ($data['userAndPartner'] as $UaP) {

					if( $log['user'] == $UaP['name'] AND $log['partner'] == $UaP['p_name']){

						// $data['reports'] = $this->Logs->get_all();
						$control = 1;
						$this->to_excel('get_all_excel','data');
						
					}
				}



			}else if($log['user'] OR $log['partner']){

				foreach ($data['userAndPartner'] as $UaP) {

					if( $log['user'] == $UaP['name'] AND $log['partner'] == $UaP['p_name']){

						// $data['reports'] = $this->Logs->get_all();
						$control = 1;
						$this->to_excel('get_all_excel','data');
						
					}

				}

				foreach ($data['userAndPartner'] as $UaP) {
					
					if( $log['user'] != $UaP['name'] AND $log['partner'] == $UaP['p_name']){
						
						// $data['reports'] = $this->Logs->get_only_partner();
						$control = 1;
						$this->to_excel('get_only_partner_excel','data');
						
					}

				}

				foreach ($data['userAndPartner'] as $UaP) {

					if( $log['user'] == $UaP['name'] AND $log['partner'] != $UaP['p_name']){
					
						// $data['reports'] = $this->Logs->get_only_user();
						$control = 1;
						$this->to_excel('get_only_user_excel','data');

					}

				}

			}else{
				$control = "";
			}


		}else{
			redirect(base_url().'manage/report_manage#error');
		}

		if ($control == "") {
			redirect(base_url().'manage/report_manage#error1');
		}else{

			// redirect(base_url().'manage/report_manage#error1');	
		}


	}

	public function to_excel($table,$filename){
		$this->load->model('Logs');	
		$result = $this->Logs->$table();

        header('Content-Disposition: attachment; filename='.$filename.'.xls');
        header('Content-type: application/force-download');
        header('Content-Transfer-Encoding: binary');
        header('Pragma: public');
        print "\xEF\xBB\xBF"; // UTF-8 BOM
        $h = array();
        foreach($result->result_array() as $row){
            foreach($row as $key=>$val){
                if(!in_array($key, $h)){
                    $h[] = $key;   
                }
            }
        }

        echo '<table><tr>';
        foreach($h as $key) {
            $key = ucwords($key);
            echo '<th>'.$key.'</th>';
        }
        echo '</tr>';

        foreach($result->result_array() as $row){
            echo '<tr>';
            foreach($row as $val){
                echo '<td>'.$val.'</td>';    
            }
        }
        echo '</tr>';
        echo '</table>';
        $control = 1;
	}


	public function most_clicked_index(){

		$this->load->view('manage/most_clicked');
	}
	public function most_clicked_pages(){
		
		$this->to_excel('get_most_clicked_pages','most_clicked_pages');

	}

	public function total_points_index(){
		$this->load->model('Partner');

        $data['users'] = $this->db->query("SELECT * FROM Partner ")->result_array();

        $this->load->view('manage/total_points',$data);
	}

	public function total_points($id1){

		$this->load->model('User');
        $this->load->model('My_Gift');
        $this->load->model('Movements');

    	$data['sales'] = $this->User->get_invoice_detail1($id1);
        $data['gifts'] = $this->My_Gift->get_list1($id1);
        $data['movements'] = $this->Movements->get_list1($id1);
        $data['campaigns'] = $this->db->query("SELECT * FROM Campaigns WHERE partner_id = '{$id1}' ")->result_array();

        $campaigns_point = 0;
        foreach ($data['campaigns'] as $cmp) {
                $campaigns_point = $cmp['score'] + $campaigns_point;
        }

        $movement=0;
        foreach ($data['movements'] as $mv) {
            // if ($mv['active'] == 1) {
                $movement = $mv['score'] + $movement;

            // }
        }

        $gift = 0;
        foreach ($data['gifts'] as $g) {
            if ($g['gActive'] == 1) {
                $gift = $g['gPoint'] + $gift;
            }
        }

        $primergy_toplam = 0;
        $eternus_toplam = 0;

        foreach ($data['sales'] as $sale) {
            if ($sale['pcid'] == 1) {
                $primergy_toplam = ($sale['Ppoint'] * $sale['piece']) + $primergy_toplam;
            }
            if ($sale['pcid'] == 2) {
                $eternus_toplam = ($sale['Ppoint'] * $sale['piece']) + $eternus_toplam;
            }
        }

        $data['primergy_toplam'] = $primergy_toplam;
        $data['eternus_toplam'] = $eternus_toplam;
        $data['movement_point'] = $movement;
        $data['kampanya_puan'] = $campaigns_point;
        $data['kullanilan_puan'] = $gift;

        $toplam = ($primergy_toplam + $eternus_toplam + $movement + $campaigns_point) - $gift;

        echo $toplam;
        

    } 

    







}


?>