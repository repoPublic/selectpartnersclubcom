<?php 

Class Partner_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
		$this->load->view('manage/partner');
	}

	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select("p_id,p_name,p_tax_office,p_tax_number");
		$this->datatables->from("Partner");
		$this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" id="remove_$1">x</a>', 'p_id');

		$list = $this->datatables->generate();
		echo $list;
	}

	public function add(){
		$post = $this->input->post(null,true);
		$this->load->model('Partner');

		$this->Partner->data = array(
									'p_name'=>$post['data']['p_name'],
									'p_tax_office'=>$post['data']['p_tax_office'],
									'p_tax_number'=>$post['data']['p_tax_number'],
									'p_address'=>$post['data']['p_address'],
									'p_tel'=>$post['data']['p_tel'],
									'p_email'=>$post['data']['p_email']
									);
		$content_id = $this->Partner->add();
		
		echo $content_id;
	}

	public function updates(){
		$post = $this->input->post(null,true);
		$this->load->model('Partner');
		$this->Partner->p_id = $post['data']['p_id'];
		$this->Partner->data = array(
									'p_name'=>$post['data']['p_name'],
									'p_tax_office'=>$post['data']['p_tax_office'],
									'p_tax_number'=>$post['data']['p_tax_number'],
									'p_address'=>$post['data']['p_address'],
									'p_tel'=>$post['data']['p_tel'],
									'p_email'=>$post['data']['p_email']
									);

		$up = $this->Partner->update();
		
		echo $up;

		// echo $update;
	}

	public function remove(){
		$post = $this->input->post(null,true);
		$this->load->model('Partner');
		$this->Partner->p_id = $post['p_id'];
		echo $this->Partner->remove();
	}

	public function edit($p_id){
		$this->load->model('Partner');
		$this->Partner->p_id = $p_id;
		$data = $this->Partner->get_edit();	

		echo json_encode($data);
	}




}


?>