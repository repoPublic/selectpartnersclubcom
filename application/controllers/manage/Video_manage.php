<?php 

Class Video_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
		$this->load->view('manage/video');
	}

	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select('id,img,title,sort',false);
		$this->datatables->from("Videos");
		$this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" id="remove_$1" onclick="confirmation()">x</a>', 'id');


		$list = $this->datatables->generate();
		$js =  json_decode($list);
		$a = array();
		foreach($js->aaData as $k=>$v){
			$v[1] = ($v[1] != '') ?  '<img src="'.base_url().'uploaded_files/'.$v[1].'" width="50" />' : '';
			$a[]=$v;
		}

		$js->aaData = $a;
		echo json_encode($js);
	}

	public function add(){
		$post = $this->input->post(null,true);
		$this->load->model('Videos');

		$this->Videos->data = array(
									'title'=>$post['data']['title'],
									'img'=>$post['data']['img'],
									'video'=>$post['data']['video'],
									'link'=>$post['data']['link'],
									'sort'=>$post['data']['sort']
									);
		$content_id = $this->Videos->add();
		
		echo $content_id;
	}

	public function updates(){
		$post = $this->input->post(null,true);
		$this->load->model('Videos');
		$this->Videos->id = $post['data']['id'];
		$this->Videos->data = array(
									'title'=>$post['data']['title'],
									'img'=>$post['data']['img'],
									'video'=>$post['data']['video'],
									'link'=>$post['data']['link'],
									'sort'=>$post['data']['sort']
									);

		$up = $this->Videos->update();
		
		echo $up;

		// echo $update;
	}

	public function remove(){
		$post = $this->input->post(null,true);
		$this->load->model('Videos');
		$this->Videos->id = $post['id'];
		echo $this->Videos->remove();
	}

	public function edit($id){
		$this->load->model('Videos');
		$this->Videos->id = $id;
		$data = $this->Videos->get_edit();	

		echo json_encode($data);
	}




}


?>