<?php 

Class User_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
		$this->load->model('Partner');
		
		$data['partner'] = $this->Partner->get_list();

		$this->load->view('manage/user',$data);
	}

	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select("id,picture,name,email,position,active");
		$this->datatables->from("User");
		$this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" id="remove_$1" onclick="confirmation()">x</a>', 'id');

		$list = $this->datatables->generate();
		$js =  json_decode($list);
		$a = array();
		foreach($js->aaData as $k=>$v){
			$v[1] = ($v[1] != '') ?  '<img src="'.base_url().'uploaded_files/'.$v[1].'" width="50" />' : '';
			$a[]=$v;
		}

		$js->aaData = $a;
		echo json_encode($js);
	}

	public function add(){
		$post = $this->input->post(null,true);
		$this->load->model('User');

		$this->User->data = array(
									'partner_id'=>$post['data']['partner_id'],
									'name'=>$post['data']['name'],
									'email'=>$post['data']['email'],
									'password'=>$post['data']['password'],
									'position'=>$post['data']['position'],
									'gsm'=>$post['data']['gsm'],
									'city'=>$post['data']['city'],
									'picture'=>$post['data']['picture'],
									'active'=>$post['data']['active']
									);
		$content_id = $this->User->add();
		
		echo $content_id;
	}

	public function updates(){
		$post = $this->input->post(null,true);
		$this->load->model('User');
		$this->User->id = $post['data']['id'];
		$this->User->data = array(
									'partner_id'=>$post['data']['partner_id'],
									'name'=>$post['data']['name'],
									'email'=>$post['data']['email'],
									'password'=>$post['data']['password'],
									'position'=>$post['data']['position'],
									'gsm'=>$post['data']['gsm'],
									'city'=>$post['data']['city'],
									'picture'=>$post['data']['picture'],
									'active'=>$post['data']['active']
									);

		$up = $this->User->update();
		
		echo $up;

		// echo $update;
	}

	public function remove(){
		$post = $this->input->post(null,true);
		$this->load->model('User');
		$this->User->id = $post['id'];
		echo $this->User->remove();
	}

	public function edit($id){
		$this->load->model('User');
		$this->User->id = $id;
		$data = $this->User->get_edit();	

		echo json_encode($data);
	}




}


?>