<?php 

Class gift_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
		$this->load->view('manage/gifts');
	}

	public function get_requests(){
		$data['talepler'] = $this->db->query("
			SELECT My_Gift.id myId, User.name as Uname, @Pname:=(SELECT Partner.p_name FROM Partner WHERE Partner.p_id = User.partner_id) as Pname, Gifts.name as gName, My_Gift.created_date as mygDate, Gifts.point as gPoint, Gifts.price as gPrice, My_Gift.active as gActive
            FROM My_Gift 
            LEFT JOIN Gifts ON My_Gift.gift_id = Gifts.id 
            LEFT JOIN User ON My_Gift.user_id = User.id
            ORDER BY mygDate DESC
		")->result_array();
		$this->load->view('manage/gift',$data);
	}

	public function change_status(){
		$this->db->where("id", $this->input->post("talep_id"));
		$update['active'] = $this->input->post('val');
		$this->db->update("My_Gift", $update);
		echo $this->db->affected_rows();
	}

	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select("id,img,name,point,price,sort");
		$this->datatables->from("Gifts");
		$this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" id="remove_$1" onclick="confirmation()">x</a>', 'id');

		$list = $this->datatables->generate();
		$js =  json_decode($list);
		$a = array();
		foreach($js->aaData as $k=>$v){
			$v[1] = ($v[1] != '') ?  '<img src="'.base_url().'uploaded_files/'.$v[1].'" width="50" />' : '';
			$a[]=$v;
		}

		$js->aaData = $a;
		echo json_encode($js);
	}

	public function add(){
		$post = $this->input->post(null,true);
		$this->load->model('Gifts');

		$this->Gifts->data = array(
									'name'=>$post['data']['name'],
									'img'=>$post['data']['img'],
									'point'=>$post['data']['point'],
									'price'=>$post['data']['price'],
									'sort'=>$post['data']['sort']
									);
		$content_id = $this->Gifts->add();
		
		echo $content_id;
	}

	public function updates(){
		$post = $this->input->post(null,true);
		$this->load->model('Gifts');
		$this->Gifts->id = $post['data']['id'];
		$this->Gifts->data = array(
									'name'=>$post['data']['name'],
									'img'=>$post['data']['img'],
									'point'=>$post['data']['point'],
									'price'=>$post['data']['price'],
									'sort'=>$post['data']['sort']
									);

		$up = $this->Gifts->update();
		
		echo $up;

		// echo $update;
	}

	public function remove(){
		$post = $this->input->post(null,true);
		$this->load->model('Gifts');
		$this->Gifts->id = $post['id'];
		echo $this->Gifts->remove();
	}

	public function edit($id){
		$this->load->model('Gifts');
		$this->Gifts->id = $id;
		$data = $this->Gifts->get_edit();	

		echo json_encode($data);
	}




}


?>