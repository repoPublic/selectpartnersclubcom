<?php 

Class Question_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
		$data['surveys'] = $this->db->query("SELECT * FROM Surveys")->result_array();
		$this->load->view('manage/question_page',$data);
	}

	public function main(){
		$data['questions'] = $this->db->query("
										SELECT 
											Questions.id as qid,
											Questions.question as qquestion, 
											Surveys.id as sid,
											Surveys.title as stitle, 
											Surveys.survey_point as survey_point
							            FROM Survey_Question_relationship as sq 
							            INNER JOIN Questions ON sq.question_id = Questions.id 
							            INNER JOIN Surveys ON sq.survey_id = Surveys.id 
							            ORDER BY Questions.id DESC
		")->result_array();

		$data['surveys'] = $this->db->query("SELECT * FROM Surveys ORDER BY id DESC")->result_array();

		$this->load->view('manage/question',$data);
	}

	public function question_answer($id){
		$data['answers'] = $this->db->query("
										SELECT 
											Answers.answer,
											@username:=(SELECT User.name FROM User WHERE User.id = Answers.user_id) as username,
											Surveys.id as sid,
											Surveys.title as stitle
							            FROM Survey_Question_relationship as sq 
							            INNER JOIN Answers ON sq.question_id = Answers.survey_question_id 
							            INNER JOIN Surveys ON sq.survey_id = Surveys.id 
							            WHERE Answers.survey_question_id = '{$id}'
							            ORDER BY Answers.id DESC
		")->result_array();

		$data['surveys'] = $this->db->query("SELECT * FROM Surveys ORDER BY id DESC")->result_array();

		$this->load->view('manage/answer',$data);
	}

	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select('Questions.id,Questions.question,(select Surveys.title from Surveys where Surveys.id = Survey_Question_relationship.survey_id)',false);
		$this->datatables->from("Questions");
		$this->datatables->join('Survey_Question_relationship', 'Survey_Question_relationship.question_id = Questions.id', 'left');
		$this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" id="remove_$1" onclick="confirmation()">x</a>', 'Questions.id');


		$list = $this->datatables->generate();
		echo $list;
	}

	public function add(){
		$post = $this->input->post(null,true);
		$this->load->model('Questions');

		$this->Questions->data = array(
									'question'=>$post['data']['question'],
									);
		$content_id = $this->Questions->add();

		$query = $this->db->query("INSERT INTO Survey_Question_relationship (question_id,survey_id) 
										  VALUES ('$content_id', '{$post['data']['survey_id']}' )
								");
		
		echo $content_id;
	}

	public function updates(){
		$post = $this->input->post(null,true);
		$this->load->model('Questions');
		$this->Questions->id = $post['data']['id'];
		$this->Questions->data = array(
									'question'=>$post['data']['question'],
									);

		$up = $this->Questions->update();

		$query = $this->db->query("UPDATE Survey_Question_relationship
									SET survey_id = '{$post['data']['survey_id']}'
									WHERE question_id = '{$post['data']['id']}' 
								");
		$new_update = $up + $query;
		echo $new_update;

	}

	public function remove(){
		$post = $this->input->post(null,true);
		$this->load->model('Questions');
		$this->Questions->id = $post['id'];

		$query = $this->db->query("DELETE FROM Survey_Question_relationship WHERE question_id = '{$post['id']}' ");

		echo $this->Questions->remove();
	}

	public function edit($id){
		$this->load->model('Questions');
		$this->Questions->id = $id;
		$data = $this->Questions->get_edit();	

		echo json_encode($data);
	}



	




}


?>