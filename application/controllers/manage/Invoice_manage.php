<?php 

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

Class Invoice_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
		$this->load->view('manage/invoice_detail');
	}

	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select("
							Invoice_Detail.piece,
							Invoices.vergi_no,
							Invoices.no,
							Invoices.created,
							Products.brand,
							Products.model,
							Products.no as Pno,
							Partner.p_name,
							(Products.point * Invoice_Detail.piece)
							");
		$this->datatables->from("Invoice_Detail");
		$this->datatables->join('Invoices', 'Invoices.no = Invoice_Detail.invoice_no', 'left');
		$this->datatables->join('Products', 'Products.no = Invoice_Detail.product_no', 'left');
		$this->datatables->join('Partner', 'Partner.p_tax_number = Invoices.vergi_no', 'left');
		// $this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" style="display:none" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" style="display:none" id="remove_$1" onclick="confirmation()">x</a>', 'id');

		$list = $this->datatables->generate();
		echo $list;
	}

	public function campaign_point_index(){
		$this->load->view('manage/campaing_point');
	}

	public function campaign_point_add(){

		$post = $this->input->post(null,true);
		$start_date = $post['start_date'];

		$partners = $this->db->query("SELECT * FROM Partner")->result_array();

		foreach ($partners as $p) {
			$query = $this->db->query("
	            SELECT inv.no as Ino, inv.created as Icreated, inv.vergi_no, pro.brand, pro.model, pro.no as Pno, pro.name as Pname, pro.points_multiplier, pro.point as Ppoint, pro.product_category_id as pcid, inD.piece, Partner.p_name as partnerName, inD.invoice_no, inv.id as invoiceID, Partner.p_tax_office, pro.id as productID, inD.product_no
	            FROM Invoice_Detail inD 
	            LEFT JOIN Products pro ON inD.product_no = pro.id 
	            LEFT JOIN Invoices inv ON inD.invoice_no = inv.no 
	            LEFT JOIN Partner ON inv.vergi_no = Partner.p_tax_number
	            WHERE Partner.p_id = '{$p['p_id']}' 
	        ");

	        $invoices = $query->result_array();


	        $kampanya_urun_id_primery_rx2540 = 163521;
	        $kampanya_urun_id_primery_rx2510 = 2222;
	        $kampanya_urun_id_eternus = 5555; 

	        $control_positive = 0;
	        //kampanya no:3, 3 adet primery rx2540 ve 1 adet eternus (start)
	        $p['counter'] = 0;
	        foreach ($invoices as $invoice) {

	        	$client_date = substr($start_date,0,7);
	        	$database_date = substr($invoice['Icreated'],0,7);
	        	if ($client_date == $database_date) {
	        		if ($invoice['vergi_no'] == $p['p_tax_number']) {
		       			if ($invoice['product_no'] == $kampanya_urun_id_eternus) {
		       				$p['counter'] =  $p['counter'] + 1;
		       				if ($control_positive == 0) {
		       					
		       					if ($p['counter'] == 1) {

			       					$p['counter'] = 0;
			       					foreach ($invoices as $invoice) {
							       		if ($invoice['vergi_no'] == $p['p_tax_number']) {
							       			if ($invoice['product_no'] == $kampanya_urun_id_primery_rx2540) {
							       				$p['counter'] =  $p['counter'] + $invoice['piece'];
							       				if ($p['counter'] == 3 ) {
							       					$comment = "3 Adet PRIMERGY RX540 Server ve Eternus DX100 Storage";
							       					$point = 2750 - 2640;

							       					$query_create = $this->db->query("
									                    INSERT INTO Campaigns (name, score, price, vergi_no, partner_id) 
									                    VALUES ('{$comment}', '2000', '12000', '{$p['p_tax_number']}', '{$p['p_id']}');
									                "); 
									                // echo $p['counter']."<br>";
									                $control_positive = 1;
							       				}
							       			}
							       		}
							       	}
			       					
			       				}

		       				}else if($control_positive == 1){

		       					if ($p['counter'] == 2) {

			       					$p['counter'] = 0;
			       					foreach ($invoices as $invoice) {
							       		if ($invoice['vergi_no'] == $p['p_tax_number']) {
							       			if ($invoice['product_no'] == $kampanya_urun_id_primery_rx2540) {
							       				$p['counter'] =  $p['counter'] + $invoice['piece'];
							       				if ($p['counter'] == 3 ) {
							       					$comment = "3 Adet PRIMERGY RX540 Server ve Eternus DX100 Storage";
							       					$point = 2750 - 2640;

							       					$query_create = $this->db->query("
									                    INSERT INTO Campaigns (name, score, price, vergi_no, partner_id) 
									                    VALUES ('{$comment}', '2000', '12000', '{$p['p_tax_number']}', '{$p['p_id']}');
									                "); 
									                // echo $p['counter']."<br>";
									                $control_positive = 1;
							       				}
							       			}
							       		}
							       	}
			       					
			       				}

		       				}

			       				
		       			}
		       		}
	        	}

	       	}
	       	//kampanya no:3, 3 adet primery rx2540 ve 1 adet eternus (finish)


	       	//kampanya no:1, 2 adet primery rx2540 ve 1 adet eternus (start)
	       	$p['counter'] = 0;
	       	foreach ($invoices as $invoice) {
	       		
	       		$client_date = substr($start_date,0,7);
	        	$database_date = substr($invoice['Icreated'],0,7);
	       		if ($client_date == $database_date) {
					if ($invoice['vergi_no'] == $p['p_tax_number']) {
		       			if ($invoice['product_no'] == $kampanya_urun_id_eternus) {
		       				$p['counter'] =  $p['counter'] + 1;
		       				if ($control_positive == 0) {
		       					
		       					if ($p['counter'] == 1) {
			       					$p['counter'] = 0;
			       					foreach ($invoices as $invoice) {
							       		if ($invoice['vergi_no'] == $p['p_tax_number']) {
							       			if ($invoice['product_no'] == $kampanya_urun_id_primery_rx2540) {
							       				$p['counter'] =  $p['counter'] + $invoice['piece'];
							       				if ($p['counter'] == 2 ) {
							       					$comment = "2 Adet PRIMERGY RX540 Server ve Eternus DX100 Storage";
							       					$point = 2000 - 2160;

							       					$query_create = $this->db->query("
									                    INSERT INTO Campaigns (name, score, price, vergi_no, partner_id) 
									                    VALUES ('{$comment}', '2000', '12000', '{$p['p_tax_number']}', '{$p['p_id']}');
									                "); 
									                $control_positive = 1;
							       				}
							       			}
							       		}
							       	}
			       					
			       				}

		       				}else if($control_positive == 1){

		       					if ($p['counter'] == 2) {
			       					$p['counter'] = 0;
			       					foreach ($invoices as $invoice) {
							       		if ($invoice['vergi_no'] == $p['p_tax_number']) {
							       			if ($invoice['product_no'] == $kampanya_urun_id_primery_rx2540) {
							       				$p['counter'] =  $p['counter'] + $invoice['piece'];
							       				if ($p['counter'] == 5 ) {
							       					$comment = "2 Adet PRIMERGY RX540 Server ve Eternus DX100 Storage";
							       					$point = 2000 - 2160;

							       					$query_create = $this->db->query("
									                    INSERT INTO Campaigns (name, score, price, vergi_no, partner_id) 
									                    VALUES ('{$comment}', '2000', '12000', '{$p['p_tax_number']}', '{$p['p_id']}');
									                "); 
							       				}
							       			}
							       		}
							       	}
			       					
			       				}

		       				}
		       			}
		       		}		
				}

	       	}
	       	//kampanya no:1, 2 adet primery rx2540 ve 1 adet eternus (finish)


	       	//kampanya no:2, 3 adet primery rx2510 ve 1 adet eternus (start)
	       	$p['counter'] = 0;
	        foreach ($invoices as $invoice) {

	        	$client_date = substr($start_date,0,7);
	        	$database_date = substr($invoice['Icreated'],0,7);
	        	if ($client_date == $database_date) {
					if ($invoice['vergi_no'] == $p['p_tax_number']) {
		       			if ($invoice['product_no'] == $kampanya_urun_id_eternus) {
		       				$p['counter'] =  $p['counter'] + 1;
		       				if ($control_positive == 0) {
		       					
		       					if ($p['counter'] == 1) {

			       					$p['counter'] = 0;
			       					foreach ($invoices as $invoice) {
							       		if ($invoice['vergi_no'] == $p['p_tax_number']) {
							       			if ($invoice['product_no'] == $kampanya_urun_id_primery_rx2510) {
							       				$p['counter'] =  $p['counter'] + $invoice['piece'];
							       				if ($p['counter'] == 3 ) {
							       					$comment = "3 Adet PRIMERGY RX510 Server ve Eternus DX100 Storage";
							       					$point = 2250 - 2640;

							       					$query_create = $this->db->query("
									                    INSERT INTO Campaigns (name, score, price, vergi_no) 
									                    VALUES ('{$comment}', '2250', '18500', '{$p['p_tax_number']}');
									                "); 
							       				}
							       			}
							       		}
							       	}
			       					
			       				}

		       				}else if($control_positive == 1){

		       					if ($p['counter'] == 2) {

			       					$p['counter'] = 0;
			       					foreach ($invoices as $invoice) {
							       		if ($invoice['vergi_no'] == $p['p_tax_number']) {
							       			if ($invoice['product_no'] == $kampanya_urun_id_primery_rx2510) {
							       				$p['counter'] =  $p['counter'] + $invoice['piece'];
							       				if ($p['counter'] == 3 ) {
							       					$comment = "3 Adet PRIMERGY RX510 Server ve Eternus DX100 Storage";
							       					$point = 2250 - 2640;

							       					$query_create = $this->db->query("
									                    INSERT INTO Campaigns (name, score, price, vergi_no) 
									                    VALUES ('{$comment}', '2250', '18500', '{$p['p_tax_number']}');
									                "); 
							       				}
							       			}
							       		}
							       	}
			       					
			       				}

		       				}
			       				
		       			}
		       		}	
				}

	       	}
	       	//kampanya no:2, 3 adet primery rx2510 ve 1 adet eternus (finish)

		}


		redirect(base_url().'manage/invoice_manage/');
	}

	public function add(){
		$post = $this->input->post(null,true);
		$this->load->model('Invoice_Detail');

		$this->Invoice_Detail->data = array(
									'title'=>$post['data']['title'],
									'img'=>$post['data']['img'],
									'img_mobile'=>$post['data']['img_mobile'],
									'link'=>$post['data']['link'],
									'sort'=>$post['data']['sort']
									);
		$content_id = $this->Invoice_Detail->add();
		
		echo $content_id;
	}

	public function fatura_import_form(){
		$this->load->view('manage/excel_import');
	}

	public function excel_import(){
		

		// Include Spout library 
		require_once 'Box/Spout/Autoloader/autoload.php';

		// check file name is not empty
		if (!empty($_FILES['file']['name'])) {
		     
		    // Get File extension eg. 'xlsx' to check file is excel sheet
		    $pathinfo = pathinfo($_FILES["file"]["name"]);
		    
		    // check file has extension xlsx, xls and also check 
		    // file is not empty
		   if (($pathinfo['extension'] == 'xlsx' || $pathinfo['extension'] == 'xls') 
		           && $_FILES['file']['size'] > 0 ) {
		        
		        // Temporary file name
		        $inputFileName = $_FILES['file']['tmp_name']; 
		   
		        // Read excel file by using ReadFactory object.
		        $reader = ReaderFactory::create(Type::XLSX);

		        // Open file
		        $reader->open($inputFileName);
		        $count = 1;
		        $rows = array(); 
		        $control_upload = "";
		        
		        // Number of sheet in excel file
		        foreach ($reader->getSheetIterator() as $sheet) {
		            
		            // Number of Rows in Excel sheet
		            foreach ($sheet->getRowIterator() as $row) {

		                // It reads data after header. In the my excel sheet, 
		                // header is in the first row. 
		                if ($count > 1) { 

		                    // Data of excel sheet
		                    $data['FaturaNumarasi'] = $row[1];
		                    $data['FaturaTarihi'] = $row[5]->format("Y/m/d");
		                    $data['VergiNumarasi'] = $row[0];


		                    $this->db->query("
							                    INSERT INTO Invoices (no, created, vergi_no) 
							                    VALUES ('{$data['FaturaNumarasi']}', '{$data['FaturaTarihi']}', '{$data['VergiNumarasi']}');
							                "); 
		                    
		                    // Push all data into array to be insert as 
		                    // batch into MySql database. 

		                    // array_push($rows, $data);
		                }
		                $count++;
		            }

		           
		            // print_r($rows);

		            // Now, here we can insert all data into database table.

		        }

		        // Close excel file
		        $reader->close();
		        $control_upload = "upload";

		    } else {

		        echo "Please Select Valid Excel File";
		    }

		} else {

		    echo "Please Select Excel File";
		    
		}

		if ($control_upload == "upload") {
			redirect(base_url().'manage/invoice_manage');
		}

	}

	public function faturalar_import_form(){
		$this->load->view('manage/excel_import_detail');
	}

	public function excel_import_detail(){
		

		// Include Spout library 
		require_once 'Box/Spout/Autoloader/autoload.php';

		// check file name is not empty
		if (!empty($_FILES['file']['name'])) {
		     
		    // Get File extension eg. 'xlsx' to check file is excel sheet
		    $pathinfo = pathinfo($_FILES["file"]["name"]);
		    
		    // check file has extension xlsx, xls and also check 
		    // file is not empty
		   if (($pathinfo['extension'] == 'xlsx' || $pathinfo['extension'] == 'xls') 
		           && $_FILES['file']['size'] > 0 ) {
		        
		        // Temporary file name
		        $inputFileName = $_FILES['file']['tmp_name']; 
		   
		        // Read excel file by using ReadFactory object.
		        $reader = ReaderFactory::create(Type::XLSX);

		        // Open file
		        $reader->open($inputFileName);
		        $count = 1;
		        $rows = array(); 
		        $control_upload = "";
		        
		        // Number of sheet in excel file
		        foreach ($reader->getSheetIterator() as $sheet) {
		            
		            // Number of Rows in Excel sheet
		            foreach ($sheet->getRowIterator() as $row) {

		                // It reads data after header. In the my excel sheet, 
		                // header is in the first row. 
		                if ($count > 1) { 

		                    // Data of excel sheet
		                    $data['FaturaNumarasi'] = $row[0];
		                    $data['UrunNumber'] = $row[3];
		                    $data['UrunAdeti'] = $row[5];
		                    $data['UrunBirimFiyati'] = $row[6];
		                    $data['UrunBirimFiyatiParaBirimi'] = $row[7];


		                    $this->db->query("
			                    INSERT INTO Invoice_Detail (piece, unit_price, currency, product_no, invoice_no) 
			                    VALUES ('{$data['UrunAdeti']}', '{$data['UrunBirimFiyati']}', '{$data['UrunBirimFiyatiParaBirimi']}', '{$data['UrunNumber']}', '{$data['FaturaNumarasi']}');
							"); 
		                    
		                    // Push all data into array to be insert as 
		                    // batch into MySql database. 

		                    // array_push($rows, $data);
		                }
		                $count++;
		            }

		           
		            // print_r($rows);

		            // Now, here we can insert all data into database table.

		        }

		        // Close excel file
		        $reader->close();
		        $control_upload = "upload";

		    } else {

		        echo "Please Select Valid Excel File";
		    }

		} else {

		    echo "Please Select Excel File";
		    
		}

		if ($control_upload == "upload") {
			redirect(base_url().'manage/invoice_manage');
		}

	}






}


?>