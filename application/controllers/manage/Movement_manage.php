<?php 

Class Movement_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
	
		$data['user'] = $this->db->query("SELECT * FROM Partner")->result_array();

		$this->load->view('manage/movement',$data);
	}
	
	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select('id,comment,score,(select Partner.p_name From Partner where Partner.p_id = Movements.partner_id)',false);
		$this->datatables->from("Movements");
		$this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" id="remove_$1" onclick="confirmation()">x</a>', 'id');


		$list = $this->datatables->generate();
		echo $list;
	}

	public function add(){
		$post = $this->input->post(null,true);
		$this->load->model('Movements');

		$this->Movements->data = array(
									'comment'=>$post['data']['comment'],
									'score'=>$post['data']['score'],
									'price'=>$post['data']['price'],
									'created_date'=>date("Y-m-d H:i:s", strtotime('+3 hours')),
									'partner_id'=>$post['data']['partner_id']
									);
		$content_id = $this->Movements->add();
		
		echo $content_id;
	}

	public function updates(){
		$post = $this->input->post(null,true);
		$this->load->model('Movements');
		$this->Movements->id = $post['data']['id'];
		$this->Movements->data = array(
									'comment'=>$post['data']['comment'],
									'score'=>$post['data']['score'],
									'price'=>$post['data']['price'],
									'created_date'=>date("Y-m-d H:i:s", strtotime('+3 hours')),
									'partner_id'=>$post['data']['partner_id']
									);

		$up = $this->Movements->update();
		
		echo $up;

	}

	public function remove(){
		$post = $this->input->post(null,true);
		$this->load->model('Movements');
		$this->Movements->id = $post['id'];
		echo $this->Movements->remove();
	}

	public function edit($id){
		$this->load->model('Movements');
		$this->Movements->id = $id;
		$data = $this->Movements->get_edit();	

		echo json_encode($data);
	}




}


?>