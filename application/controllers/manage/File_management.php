<?
class File_management extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	function upload_file(){
        // $this->load->helper("url");
		if (!empty($_FILES)) {
                  $tempFile = $_FILES['Filedata']['tmp_name'];
                  $targetPath = $_SERVER['DOCUMENT_ROOT'] . "/" . $this->config->item("upload_path") . 'uploaded_files/';

                  // print_r($_FILES['Filedata']);

                  $pathInfo = pathinfo($_FILES['Filedata']['name']);

                  // $ext = explode('.', $_FILES['Filedata']['name']);

                  // // print_r($ext);
                  // exit();

                  $fileName = url_title($pathInfo['filename']) ."." . $pathInfo['extension'];
                  $targetFile =  str_replace('//','/',$targetPath) . $fileName;

                  move_uploaded_file($tempFile,$targetFile);

            
                  $data['filename'] = $fileName;
                  $data['path'] = $pathInfo['extension'];
                  echo json_encode($data);
                  
                  /*
                  $this->load->model('File');
                  $this->File->file_name = $fileName;
                  $this->File->file_type = $type;
      			*/
            }
	}

	function delete_file(){
            check_access("files", true);
		$this->load->model('File');
		$this->File->file_id = $this->input->post("file_id");
		echo $this->File->remove_file();
	}

}

?>