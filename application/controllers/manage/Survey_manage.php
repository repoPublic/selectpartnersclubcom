<?php 

Class Survey_manage extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata("admin")){
			redirect(base_url().'manage/login');
		}
	}

	public function index(){
		$this->load->view('manage/survey');
	}
	
	public function get_list(){
		$this->load->library('datatables');
		$this->datatables->select('id,title,survey_point,survey_price,active',false);
		$this->datatables->from("Surveys");
		$this->datatables->add_column('Sil', '<a class="btn btn-warning btn-sm edit" id="edit_$1" data-page-id="new_proc" style="float:left;">düzenle</a><a class="btn btn-danger btn-sm remove" id="remove_$1" onclick="confirmation()">x</a>', 'id');


		$list = $this->datatables->generate();
		echo $list;
	}

	public function add(){
		$post = $this->input->post(null,true);
		$this->load->model('Surveys');

		$this->Surveys->data = array(
									'title'=>$post['data']['title'],
									'survey_point'=>$post['data']['survey_point'],
									'survey_price'=>$post['data']['survey_price'],
									'active'=>$post['data']['active']
									);
		$content_id = $this->Surveys->add();
		
		echo $content_id;
	}

	public function updates(){
		$post = $this->input->post(null,true);
		$this->load->model('Surveys');
		$this->Surveys->id = $post['data']['id'];
		$this->Surveys->data = array(
									'title'=>$post['data']['title'],
									'survey_point'=>$post['data']['survey_point'],
									'survey_price'=>$post['data']['survey_price'],
									'active'=>$post['data']['active']
									);

		$up = $this->Surveys->update();
		
		echo $up;

	}

	public function remove(){
		$post = $this->input->post(null,true);
		$this->load->model('Surveys');
		$this->Surveys->id = $post['id'];
		echo $this->Surveys->remove();
	}

	public function edit($id){
		$this->load->model('Surveys');
		$this->Surveys->id = $id;
		$data = $this->Surveys->get_edit();	

		echo json_encode($data);
	}




}


?>