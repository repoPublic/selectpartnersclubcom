<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        parent::__construct();
    }

	public function index()
	{   
		$this->load->model('Blogs');
		$this->load->model('Videos');
		$this->load->model('Sliders');
		$this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Anasayfaya girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $data['videos'] = $this->Videos->get_list();
        $data['sliders'] = $this->Sliders->get_list();
		
		$query = $this->db->query("SELECT 
									Questions.id as qid, Questions.question
									FROM 
									Survey_Question_relationship as sq 
									INNER JOIN Questions ON sq.question_id = Questions.id 
									INNER JOIN Surveys ON sq.survey_id = Surveys.id
									WHERE Surveys.active = 1
								");
		$data['questions'] = $query->result_array();

		$data['blogs'] = $this->Blogs->get_list();

		$this->load->view('home',$data);
	}

	public function answer_add(){
		$this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Anket formu kayıt edildi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

		$answers = $this->input->post();
		foreach ($answers as  $key=>$answer) {
			 $question = explode("-", $key)[1];

			 $query = $this->db->query("INSERT INTO Answers (answer,user_id,survey_question_id) VALUES ('$answer', '{$_SESSION["id"]}', '$question')");
		}

		$mysql_date_now = date("Y-m-d H:i:s", strtotime('+3 hours'));

		$data['survey_query'] = $this->db->query("SELECT * FROM Surveys")->result_array();

		foreach ($data['survey_query'] as $survey) {
			if ($survey['active'] == 1) {

				$query_user = $this->db->query("SELECT 
		                                        * 
		                                        FROM Movements 
		                                        WHERE user_id = '{$_SESSION["id"]}' AND 
		                                              comment = '{$survey["title"]} isimli Anket kayıt edildi'
		                                               ")->result_array();

		        $query_partner = $this->db->query("SELECT 
		                                        * 
		                                        FROM Movements 
		                                        WHERE 
		                                            comment = '{$survey["title"]} isimli Anket kayıt edildi' AND
		                                            partner_id = '{$_POST["partner_id"]}'
		                                               ")->result_array();

				if (!$query_user) {
        	
            		if (!$query_partner) {
						$query1 = $this->db->query("INSERT INTO Movements (comment,score,price,created_date,user_id, partner_id) 
														  VALUES ('{$survey["title"]} isimli Anket kayıt edildi', '{$survey["survey_point"]}', '{$survey["survey_price"]}', '{$mysql_date_now}', '{$_SESSION["id"]}', '{$_SESSION["partner_id"]}' )
												");

					}
				}


			}
		}

		redirect(base_url());
	
	}

	public function rssoku() 
	{   
	    $feed = file_get_contents("http://tr.fujitsu-news.com/feed/");
	    $xml= new SimpleXMLElement($feed);
	    $sayac="1";
	    $limit="10";

	    foreach ($xml -> channel -> item as $veri){
	        if ($sayac <= $limit){ 

	        $html_all = $veri -> children('content', true)->encoded;
	        $dom = new DOMDocument();
		    libxml_use_internal_errors(true);
		    $dom->loadHTML($html_all);
		    libxml_clear_errors();
	        $image = $dom->getElementsByTagName('img')->item(0)->getAttribute('src');

	        $desc= $veri -> description;
	        $desc=substr($desc,0,250);
	        $link = $veri -> link;
	        $title= $veri -> title;
	        $date = $veri -> pubDate;

	        //$script = $veri -> script;

	        //$deger = explode(",", $script);
	        //$image = str_replace("'", "", $deger["60"]);

	        $bul = array("Wed,", "Mon,", "Tue,", "Thu,", "Fri,", "Sat,", "Sun,");
	        $tarih = str_replace($bul, "", $date);

	        $tarih = str_replace("Jan", "/ 01 /", $tarih);
	        $tarih = str_replace("Feb", "/ 02 /", $tarih);
	        $tarih = str_replace("Mar", "/ 03 /", $tarih);
	        $tarih = str_replace("Apr", "/ 04 /", $tarih);
	        $tarih = str_replace("May", "/ 05 /", $tarih);
	        $tarih = str_replace("Jun", "/ 06 /", $tarih);
	        $tarih = str_replace("Jul", "/ 07 /", $tarih);
	        $tarih = str_replace("Aug", "/ 08 /", $tarih);
	        $tarih = str_replace("Sep", "/ 09 /", $tarih);
	        $tarih = str_replace("Oct", "/ 10 /", $tarih);
	        $tarih = str_replace("Nov", "/ 11 /", $tarih);
	        $tarih = str_replace("Dec", "/ 12 /", $tarih);
	        $tarih=substr($tarih,0,15);

	        $baslik = str_replace("ÄŸ", "ğ", $title); 
	        $baslik = str_replace("Äz", "Ğ", $baslik); 
	        $baslik = str_replace("Ã¼", "ü", $baslik); 
	        $baslik = str_replace("Ãœ", "Ü", $baslik); 
	        $baslik = str_replace("ÅŸ", "ş", $baslik); 
	        $baslik = str_replace("Åz", "Ş", $baslik); 
	        $baslik = str_replace("Ä°", "İ", $baslik); 
	        $baslik = str_replace("Ä±", "ı", $baslik); 
	        $baslik = str_replace("Ã¶", "ö", $baslik); 
	        $baslik = str_replace("Ã–", "Ö", $baslik); 
	        $baslik = str_replace("Ã§", "ç", $baslik); 
	        $baslik = str_replace("Ã‡", "Ç", $baslik); 
	        $baslik = str_replace("â€“", "-", $baslik);
	        $baslik = str_replace("â€˜ ", "'", $baslik);
	        $baslik = str_replace("â€²", "'", $baslik);
	         
	        $aciklama = str_replace("ÄŸ", "ğ", $desc); 
	        $aciklama = str_replace("Äz", "Ğ", $aciklama); 
	        $aciklama = str_replace("Ã¼", "ü", $aciklama); 
	        $aciklama = str_replace("Ãœ", "Ü", $aciklama); 
	        $aciklama = str_replace("ÅŸ", "ş", $aciklama); 
	        $aciklama = str_replace("Åz", "Ş", $aciklama); 
	        $aciklama = str_replace("Ä°", "İ", $aciklama); 
	        $aciklama = str_replace("Ä±", "ı", $aciklama); 
	        $aciklama = str_replace("Ã¶", "ö", $aciklama); 
	        $aciklama = str_replace("Ã–", "Ö", $aciklama); 
	        $aciklama = str_replace("Ã§", "ç", $aciklama); 
	        $aciklama = str_replace("Ã‡", "Ç", $aciklama); 
	        $aciklama = str_replace("â€“", "-", $aciklama);
	        $aciklama = str_replace("â€˜ ", "'", $aciklama);
	        $aciklama = str_replace("â€²", "'", $aciklama);


	        $query_blog = $this->db->query("SELECT * FROM Blogs WHERE title = '{$baslik}' ")->row_array();
	        
        	if ($query_blog['title'] != $baslik) {
	        	$this->db->query("INSERT INTO Blogs (title, image , detail, link, created_date, blog_score, price) 
        						    VALUES ('$baslik', '$image', '$aciklama', '$link', '$tarih', 1,  1 )");
	        }
	        echo $image."<br />";
	        // echo "
	        //     <html>
	        //     <head>
	        //     <title>rss</title>

	        //     </head>
	        //     <body>
	        //     <div class='konular'>
	        //     <li><div class='baslik'><a target=\"_blank\" rel=\"nofollow\" href=\"$link\" title=\"$baslik\">$baslik</a></div><br />
	        //     <p>$aciklama ...</p><br>$tarih</li>
	        //     </div>
	        //     </body>
	        //     </html>
	        // ";
	         
	        }
	    $sayac++;
	    }
	} 


	public function webinar()
	{   
		$this->load->model('Blogs');
		$this->load->model('Videos');
		$this->load->model('Sliders');
		$this->load->model('Logs');
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Anasayfaya girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $data['videos'] = $this->Videos->get_list();
        $data['sliders'] = $this->Sliders->get_list();
		
		$query = $this->db->query("SELECT 
									Questions.id as qid, Questions.question
									FROM 
									Survey_Question_relationship as sq 
									INNER JOIN Questions ON sq.question_id = Questions.id 
									INNER JOIN Surveys ON sq.survey_id = Surveys.id
									WHERE Surveys.active = 1
								");
		$data['questions'] = $query->result_array();

		$data['blogs'] = $this->Blogs->get_list();

		$this->load->view('home',$data);
	}
	

}
