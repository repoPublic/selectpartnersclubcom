<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Logs');
    }

    public function index()
    {   
        $this->Logs->ip_address = $this->input->ip_address();
        $this->Logs->user_id = $this->session->id;
        $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
        $this->Logs->comment = "Şifre değiştirme sayfasına girdi";
        $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (isset($this->session->id)) {
            $this->Logs->get_insert();
        }

        $this->load->view('password_change');
    }

    public function change(){

        $query = $this->db->query("SELECT * FROM User WHERE email = '{$_POST['email']}' AND  active = 1 ");
        $result = $query->row_array();

        if (isset($result['id'])) {

            $this->Logs->ip_address = $this->input->ip_address();
            $this->Logs->user_id = $this->session->id;
            $this->Logs->created_date = date("Y-m-d H:i:s", strtotime('+3 hours'));
            $this->Logs->comment = "Şifre değiştirdi";
            $this->Logs->page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            if (isset($this->session->id)) {
                $this->Logs->get_insert();
            }

            $body = '<html><head><meta charset="utf-8" /></head><body><table cellpadding="0" cellspacing="0" border="0"><tr><td colspan="3"><b>Sifresini Değistirmek İsteyen Kullanıcı</b></td></tr><tr><td>Mail Adresi</td><td>:</td><td>'.$_POST['email'].'</td></tr></table></body></html>';

            $postData = array(
            'from_email'=>'GOKSAN.KADAYIFCI.external@ts.fujitsu.com',
            'from_name'=>'Select Partners Club',
                       'to' => "ezgi.kumbuloglu@great.com.tr",
                       'subject' => 'Select Partners Club Sifre Değistirme İsteği',
                       'content' => $body
                   );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"http://www.ballsurance.com:3111/send");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec ($ch);
            curl_close ($ch);

            echo json_encode($result['email']);

        }else{
            echo "Pfalse";
        }


    }
}
