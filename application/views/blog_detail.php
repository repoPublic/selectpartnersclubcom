<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<body>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="page-title">BLOG</div>
                </div>
            </div>
            <div class="row">

                
                <div class="col-md-9 col-sm-9 col-xs-12">
                        
                    <div class="blog-boxes">

                        <div class="blog-boxesImg"><img src="<?php echo $blog_detail['image']; ?>" class="img-responsive" ></div>

                        <div class="blog-boxesContent">

                            <div class="row">

                                <div class="col-md-8 col-sm-8 col-xs-12 blog-boxesTitle" partner_id="<?php echo $_SESSION["partner_id"]; ?>" ><?php echo $blog_detail['title']; ?></div>

                                <div class="col-md-4 col-sm-4 col-xs-12 blog-boxesDate"><?php echo $blog_detail['created_date']; ?></div>

                            </div>

                            <div class="blog-boxesText">
                                
                            <?php
                                echo $blog_detail['detail'];
                            ?>

                            </div>

                            <div class="row">

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="blog-socialBox" style="display: none;">
                                    
                                        <a href=""><img src="<?php echo base_url();?>assets/images/fb-icon.png"></a>

                                        <a href=""><img src="<?php echo base_url();?>assets/images/tw-icon.png"></a>

                                        <a href=""><img src="<?php echo base_url();?>assets/images/gplus-icon.png"></a>

                                        <a href=""><img src="<?php echo base_url();?>assets/images/in-icon.png"></a>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="blog-tag" style="display: none;">
                            
                            <img src="<?php echo base_url();?>assets/images/tag-icon.png">

                            <p>Abhorreant mea, Hinc eruditi mea at, Usu ne debet ignota, At mel ullum, Aeque</p>

                        </div>

                    </div> 


                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                    
                    <div class="blog-rightBox">
                        <form name="blogSearchForm" method="POST" action="<?php echo base_url();?>blog/search" >
                            <input type="text" name="search_text" class="blog-searchInput" placeholder="Ara">

                            <input type="submit" value="" class="blog-searchSubmit">
                        </form>
                    </div>

                    <div class="blog-rightBox">

                        <div class="blog-rightTitle">SON PAYLAŞIMLAR</div>

                        <ul class="blog-rightList">
                            <?php foreach ($blog_5 as $five): ?>
                                <li><a href="<?php echo base_url();?>blog/detail/<?php echo $five['id']; ?>"><?php echo $five['title']; ?></a></li>    
                            <?php endforeach ?>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>

<script type="text/javascript">
    
    $(document).ready(function(){
        var partner_id = $('.blog-boxesTitle').attr('partner_id');
        var comment_value = $('.blog-boxesTitle').text();
        var comment = 'Blog okundu. (Blog başlığı: ' + comment_value + ')';
        var point = <?php echo $blog_detail['blog_score']; ?>;
        var price = <?php echo $blog_detail['price']; ?>;
        var count = 30;
        countdown = setInterval(function(){
            // console.log(count);
            if (count == 0) {
                $.ajax({
                    url: base_url + "movement/point_add",
                    dataType: 'json',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {comment: comment, score : point, price : price, partner_id : partner_id},
                });
                clearInterval(countdown);
            }
            count--;
        }, 1000);
    });

</script>

</body>
</html>