<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>

<body class="login">

<div class="content">
    <div class="container">
        <form class="form-signin" method="post" >
            <div class="loginLogo"><img src="<?php echo base_url();?>assets/images/white-logo.png"  class="img-responsive" style="display: inline-block;"/></div>
            <div class="iconTitle">
                <span>
                    <i class="fa fa-user"></i>
                </span>
            </div>
            <h2 class="form-signin-heading">Şifremi Unuttum</h2>
            
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="email" required="" autofocus="">
            
            <a class="btn btn-lg btn-primary btn-block passChange" >Gönder</a>

        </form>
    </div>
</div>
<?php include "inc/script.php"; ?>


<script type="text/javascript">
    $(document).ready(function(){

        $('.passChange').click(function(){

            var email = $('#inputEmail').val();

            $.ajax({
                url: "<?php echo base_url(); ?>" + "password/change",
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: {email: email},
                success:function( data, textStatus, jQxhr ) {
                    if (data == "Pfalse") {
                        alert("Bu mail adresine kayıtlı kullanıcı bulunmamaktadır. Lütfen tekrar deneyiniz.");
                    }else{
                        alert("Şifre Değiştirme İsteği Başarılı Bir Şekilde Gerçekleştirildi. En Kısa Zamanda Geri Dönüş Yapılacaktır.");
                        window.location.pathname = "/";
                    }
                },
                error:function( jqXhr, textStatus, errorThrown){
                     console.log(errorThrown);
                }
            });

        });

    });
</script>

</body>
</html>