<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>

<body class="login">

<div class="content">
    <div class="container">
        <form class="form-signin" method="post">
            <div class="loginLogo"><img src="<?php echo base_url();?>assets/images/white-logo.png"  class="img-responsive" style="display: inline-block;"/></div>
            <div class="iconTitle">
                <span>
                    <i class="fa fa-user"></i>
                </span>
            </div>
            <h2 class="form-signin-heading">Giriş Yapın</h2>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="email" required="" autofocus="">
            <input type="password" name="pw" id="inputPassword" class="form-control" placeholder="*******" required="">
           <!--  <div class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> <i>Beni Hatırla</i>
                </label>
            </div> -->
            <a href="<?php echo base_url();?>password"><i>Şifrenizi mi unuttunuz?</i></a>
            <a class="btn btn-lg btn-primary btn-block webLogin" >Giriş</a>
        </form>
    </div>
</div>
<?php include "inc/script.php"; ?>

<script type="text/javascript">
    $(document).ready(function(){

        $('.webLogin').click(function(){

            var email = $('#inputEmail').val();
            var password = $('#inputPassword').val();

            $.ajax({
                url: "<?php echo base_url(); ?>" + "login/validate",
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: {email: email, password: password},
                success:function( data, textStatus, jQxhr ) {
                    if (data == "Lfalse") {
                        alert("Email veya Şifreniz yanlış girilmiş olabilir. Tekrar Deneyiniz.");
                    }else{
                        window.location.pathname = "/";
                    }
                },
                error:function( jqXhr, textStatus, errorThrown){
                     console.log(errorThrown);
                }
            });

        });

    });
</script>

</body>
</html>