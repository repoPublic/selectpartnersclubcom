<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<body>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <?php include 'inc/profil-left.php'; ?>

                <div class="col-md-9 col-sm-12 col-xs-12">
                    
                    <div class="katilim-right profil-right">
                    <form action="<?php echo base_url();?>profil/profil_update/<?php echo $profil['id']; ?>" method="post">

                        <?php include 'inc/profil-top.php'; ?>

                        <div class="profil-graph" style="margin-bottom: 10px;">

                            <div class="graphBoxes-title">PROFİL</div>

                        </div>

                        <div class="katilim-inputRow">

                            <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Vergi Dairesi</div></div>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <p class="font-size14"><?php echo $profil['p_tax_office']; ?></p>
                            </div>
                            
                        </div>

                        <div class="katilim-inputRow">

                            <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Vergi Numarası</div></div>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <p class="font-size14"><?php echo $profil['p_tax_number']; ?></p>
                            </div>
                            
                        </div>

                        <div class="katilim-inputRow">

                            <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Firma Adresi</div></div>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <p class="font-size14"><?php echo $profil['p_address']; ?></p>
                            </div>
                            
                        </div>

                        <div class="katilim-inputRow">

                            <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Firma Telefonu</div></div>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <p class="font-size14"><?php echo $profil['p_tel']; ?></p>
                            </div>
                                
                        </div>

                        <div class="katilim-inputRow">

                            <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Firma E-posta</div></div>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <p class="font-size14"><?php echo $profil['p_email']; ?></p>
                            </div>
                            
                        </div>

                        <div class="katilim-inputRow">

                            <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Firmadaki Görevi</div></div>

                            <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Firmadaki_Gorevi" value="<?php echo $profil['position']; ?>"></div>
                            
                        </div>
                        
                        <div class="katilim-inputRow">

                            <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">E-posta</div></div>

                            <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="E_posta" value="<?php echo $profil['email']; ?>"></div>
                            
                        </div>

                        <div class="katilim-inputRow">

                            <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Kullanıcı Telefonu</div></div>

                            <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Kullanici_Telefonu" value="<?php echo $profil['gsm']; ?>"></div>
                            
                        </div>

                        <div class="katilim-inputRow" style="margin-top: 35px;">

                            <div class="col-md-3 col-sm-3 col-xs-12"></div>

                            <div class="col-md-9 col-sm-9 col-xs-12"><input type="submit" class="katilim-submit" value="Bilgileri Güncelle"></div>
                            
                        </div>

                    </form>
                    </div>                    

                </div>
            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>
</body>
</html>