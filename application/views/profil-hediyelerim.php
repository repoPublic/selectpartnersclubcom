<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<body>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <?php include 'inc/profil-left.php'; ?>

                <div class="col-md-9 col-sm-12 col-xs-12">
                    
                    <div class="profil-right">

                        <?php include 'inc/profil-top.php'; ?>
                        
                        <div class="profil-graph">
                            
                            <div class="graphBoxes-title">HEDİYELERİM</div>

                            <div class="profil-giftBox">

                                <table width="100%" border="0" class="giftTable">
                                    <thead>
                                        <tr>
                                            <th width="50%">Hediye</th>
                                            <th>Tarih</th>
                                            <th>Durum</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($gifts as $gift): ?>
                                        <tr>
                                            <td><?php echo $gift['gName']; ?></td>
                                            <td><?php echo $gift['mygDate']; ?></td>
                                            <td><?php echo $gift['gActive'] == '1' ? 'Aktif' : 'Pasif'; ?></td>
                                        </tr>    
                                    <?php endforeach ?>
                                    </tbody>                                        
                                    
                                </table>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>
</body>
</html>