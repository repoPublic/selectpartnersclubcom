<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<body>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="page-title">PAZARLAMA DOKÜMANLARI</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="branding-box">

                        <a class="branding-head" data-target="#logo">Logo<span ></span></a>

                        <span class="branding-spot"></span>

                        <a class="branding-head" data-target="#klavuz">Kurumsal Kimlik ve Klavuzlar<span></span></a>

                        <span class="branding-spot"></span>

                        <!-- <a class="branding-head" data-target="#dergiler">Dergiler<span ></span></a>

                        <span class="branding-spot"></span> -->

                        <a class="branding-head" data-target="#mailingler">Mailingler<span class="active"></span></a>


                        <div class="branding-boxes" id="logo" >

                            <div class="container">

                                <div class="branding-area">
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <img src="<?php echo base_url();?>assets/images/branding/fujitsu_partner_logo.png" id="logo-document" class="img-responsive" />
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="document-content">
                                            <div class="document-title-logo document-title-main">
                                                <span class="preview active-red" data-preview="<?php echo base_url();?>assets/images/branding/fujitsu_partner_logo.png">• PDF</span> 
                                                    <a href="<?php echo base_url();?>assets/docs/32210_Co-Branding_Logo__Generic.pdf" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                            <div class="document-title-logo document-title-main">
                                                <span class="preview " data-preview="<?php echo base_url();?>assets/images/branding/fujitsu_partner_logo.png">• JPG</span> 
                                                    <a href="<?php echo base_url();?>assets/docs/buyuk.jpg" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                            <div class="document-title-logo document-title-main">
                                                <span class="preview " data-preview="<?php echo base_url();?>assets/images/branding/fujitsu_partner_logo.png">• EPS</span> 
                                                    <a href="<?php echo base_url();?>assets/docs/smwbr.eps" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                            <div class="document-title-logo document-title-main">
                                                <span class="preview" data-preview="<?php echo base_url();?>assets/images/branding/fujitsu_partner_logo.png">• AI</span> 
                                                    <a href="<?php echo base_url();?>assets/docs/PartnerLogo_Generic_RZ.ai" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                            <div class="document-title-logo document-title-main">
                                                <span class="preview" data-preview="<?php echo base_url();?>assets/images/branding/fujitsu_partner_logo.png">• ICO</span> 
                                                    <a href="<?php echo base_url();?>fuji_favicon.ico" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5 col-sm-5 col-xs-12">

                                    </div>
                                </div>

                            </div>
                            
                        </div>

                        <div class="branding-boxes" id="klavuz">

                            <div class="container">

                                <div class="branding-area">
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <img src="<?php echo base_url();?>assets/images/branding/klavuz.png" id="klavuz-document" class="img-responsive" />
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="document-content">
                                            <div class="document-title-klavuz document-title-main">
                                                <span class="preview active-red" data-preview="<?php echo base_url();?>assets/images/branding/klavuz.png">• Kurumsal Kimlik</span> 
                                                    <a href="<?php echo base_url();?>assets/docs/Branding_Guideline.pdf" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>

                                            <div class="document-title-klavuz document-title-main">
                                                <span class="preview " data-preview="<?php echo base_url();?>assets/images/branding/fujitsu_partner_logo.png">• System Architecht Kullanım Kılavuzu</span> 
                                                    <a href="<?php echo base_url();?>assets/docs/system_architecht_kullaninim_kilavuzu.pdf" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-5 col-sm-5 col-xs-12">

                                    </div>
                                </div>

                            </div>
                            
                        </div>

                        <div class="branding-boxes" id="dergiler">

                            <div class="container">

                                <div class="col-md-4 col-sm-4 col-xs-12">

                                </div>

                                <div class="col-md-8 col-sm-8 col-xs-12">

                                </div>

                            </div>
                            
                        </div>

                        <div class="branding-boxes" id="mailingler" style="height: auto;">

                            <div class="container">

                                <div class="branding-area">
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <img src="<?php echo base_url();?>assets/images/branding/3lu_server.png" id="mailing-document" class="img-responsive" />
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="document-content">
                                            <!--<div class="document-title document-title-main">
                                                <span class="preview active-red" data-preview="<?php echo base_url();?>assets/images/branding/prag_tatil.png">• Fujitsu Prag Tatil</span> 
                                                    <a href="<?php echo base_url();?>assets/images/branding/prag_tatil.png" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                            <div class="document-title document-title-main">
                                                <span class="preview" data-preview="<?php echo base_url();?>assets/images/branding/Esprimo-Kampanya.png">• Fujitsu Esprimo Kampanya</span> 
                                                    <a href="<?php echo base_url();?>assets/images/branding/Esprimo-Kampanya.png" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                            <div class="document-title document-title-main">
                                                <span class="preview" data-preview="<?php echo base_url();?>assets/images/branding/altin_firsat.png">• Fujitsu Altın Fırsat</span>
                                                    <a href="<?php echo base_url();?>assets/images/branding/altin_firsat.png" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                            <div class="document-title document-title-main">
                                                <span class="preview active-red" data-preview="<?php echo base_url();?>assets/images/branding/3lu_server.png">• Fujitsu Üçlü Primergy Server Storage</span>  
                                                    <a href="<?php echo base_url();?>assets/images/branding/3lu_server.png" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>-->
                                            <div class="document-title document-title-main">
                                                <span class="preview active-red" data-preview="<?php echo base_url();?>assets/images/branding/vespa.png">• Fujitsu Vespa Kampanya </span>
                                                    <a href="<?php echo base_url();?>assets/images/branding/vespa.png" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                            <div class="document-title document-title-main">
                                                <span class="preview" data-preview="<?php echo base_url();?>assets/images/branding/etstur.png">• Fujitsu ETSTur Kampanya </span>
                                                    <a href="<?php echo base_url();?>assets/images/branding/etstur.png" download target="_blank" >
                                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                                    </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5 col-sm-5 col-xs-12">

                                    </div>
                                </div>

                            </div>
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>
<script type="text/javascript">
    $('.branding-head').click(function () {
        $('.branding-head span').removeClass("active");
        $(this).find('span').addClass('active');
        var target = $(this).attr('data-target');
        var curHeight = $(target).find('.container').height();
        $('.branding-boxes').stop().animate({height: 0}, 100);
        $(target).stop().animate({height: curHeight}, 200);
    });

    $('.document-title .preview').click(function(){
        $('.document-title span').removeClass("active-red");
        $(this).addClass('active-red');
        var p = $(this).attr('data-preview');
        $('#mailing-document').attr('src',p);
    });

    $('.document-title-klavuz .preview').click(function(){
        $('.document-title-klavuz span').removeClass("active-red");
        $(this).addClass('active-red');
        var p = $(this).attr('data-preview');
        $('#klavuz-document').attr('src',p);
    });

    $('.document-title-logo .preview').click(function(){
        $('.document-title-logo span').removeClass("active-red");
        $(this).addClass('active-red');
        var p = $(this).attr('data-preview');
        $('#logo-document').attr('src',p);
    });
</script>
</body>
</html>






