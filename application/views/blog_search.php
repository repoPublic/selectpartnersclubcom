<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<body>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="page-title">BLOG</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 col-sm-1 col-xs-12" style="display: none;">

                    <ul class="blog-yearList">
                        <li><a href="" class="active">2016</a></li>
                        <li><a href="">2015</a></li>
                        <li><a href="">2014</a></li>
                        <li><a href="">2013</a></li>
                    </ul>

                </div>

                
                <div class="col-md-9 col-sm-9 col-xs-12">

                    <!-- blog foreach top -->

                    <?php foreach ($blog_search as $blog): ?>
                        
                    <div class="blog-boxes" style="display: block;">

                        <div class="blog-boxesImg">
                            <?php if ($blog['image']): ?>
                                <img src="<?php echo $blog['image']; ?>" class="img-responsive" >
                            <?php endif ?>
                        </div>

                        <div class="blog-boxesContent">

                            <div class="row">

                                <div class="col-md-8 col-sm-8 col-xs-12 blog-boxesTitle"><?php echo $blog['title']; ?></div>

                                <div class="col-md-4 col-sm-4 col-xs-12 blog-boxesDate"><?php echo $blog['created_date']; ?></div>

                            </div>

                            <div class="blog-boxesText">
                                
                            <?php
                                echo $blog['detail']; 
                            ?> ...

                            </div>

                            <div class="row">

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <a href="<?php echo $blog['link']; ?>" target="_blank" class="blog-boxesMore">Devamını Oku...</a>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="blog-socialBox" style="display: none;">
                                    
                                        <a href=""><img src="<?php echo base_url();?>assets/images/fb-icon.png"></a>

                                        <a href=""><img src="<?php echo base_url();?>assets/images/tw-icon.png"></a>

                                        <a href=""><img src="<?php echo base_url();?>assets/images/gplus-icon.png"></a>

                                        <a href=""><img src="<?php echo base_url();?>assets/images/in-icon.png"></a>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="blog-tag" style="display: none;">
                            
                            <img src="<?php echo base_url();?>assets/images/tag-icon.png">

                            <p>Abhorreant mea, Hinc eruditi mea at, Usu ne debet ignota, At mel ullum, Aeque</p>

                        </div>

                    </div> 

                    <?php endforeach ?>

                    <!-- blog foreach bottom -->


                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                    
                    <div class="blog-rightBox">
                        <form name="blogSearchForm" method="POST" action="<?php echo base_url();?>blog/search" >
                            <input type="text" name="search_text" class="blog-searchInput" placeholder="Ara">

                            <input type="submit" value="" class="blog-searchSubmit">
                        </form>
                    </div>

                    <div class="blog-rightBox">

                        <div class="blog-rightTitle">SON PAYLAŞIMLAR</div>

                        <ul class="blog-rightList">
                            <?php foreach ($blog_5 as $five): ?>
                                <li><a href="<?php echo base_url();?>blog/detail/<?php echo $five['id']; ?>"><?php echo $five['title']; ?></a></li>    
                            <?php endforeach ?>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>
</body>
</html>