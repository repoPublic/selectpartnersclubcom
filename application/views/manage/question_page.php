<?php include "head.php";?>

	<?php include "nav.php";?>	
	<script type="text/javascript">
		$(document).ready(function(){
			Question.init();

		});

	

	</script>
	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">
		<div class="fade-page" id="new_proc">
			<form role="form" id="forms">
				<input type="hidden" name="id" />
				
				<div class="form-group">
					<label>Soru</label>
					<input type="text" class="form-control" name="question" >
				</div>

				<div class="form-group">
					<label>Anket Seçiniz</label>
					<select class="form-control" name="survey_id">
						<option disabled selected id="sec">Seçiniz</option>
						<?php foreach($surveys as $v){ 
							echo '<option value="'.$v['id'].'" id="survey_'.$v['id'].'">'.$v['title'].'</option>'; } 
						?>
					</select>
				</div>

				<a class="btn btn-default btn-sm new" id="inpt-btn">Ekle</a>
				<a class="btn btn-danger btn-sm" href="<?php echo base_url();?>manage/question_manage">Vazgeç</a>
			</form>
		</div>
		<div class="fade-page" style="display:block;">

			

			<a class="btn btn-sm btn-warning input-buttons" data-page-id="new_proc">Yeni Kayıt</a>
			<!-- <a class="btn btn-success btn-sm" href="<?php echo base_url();?>manage/blogCat_manage">Kategori Ekle</a> -->
			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th style="width:120px;">ID</th>
						<th>Soru</th>
						<th>Anket</th>
						<th style="width:120px;"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>