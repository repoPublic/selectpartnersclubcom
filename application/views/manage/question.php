<?php include "head.php";?>

	<?php include "nav.php";?>	
	<script type="text/javascript">
		$(document).ready(function(){

			$("#filtrele").change(function(){
				var val = $(this).val();
				$("#list tbody tr").each(function(ind, elem){
					
					if(val == "-1"){
						$(elem).show();
					}else{
						if($(elem).find("#survey_title").attr('data-title') != val){
							$(elem).hide();
						}else{
							$(elem).show();
						}
					}
				});
			});

		});
	
	</script>
	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">


		<div class="fade-page" style="display:block;">

			<a class="btn btn-sm btn-success input-buttons pull-left" href="<?php echo base_url();?>manage/question_manage">Soru Ekle</a>

			<div style="float:left;margin-top: 40px;left: -72px;position: relative;">
				<select id="filtrele">
					<option value="-1">Anket Seçiniz</option>
					<?php foreach ($surveys as $survey): ?>
						<option value="<?=$survey['id']?>"><?=$survey['title']?></option>
					<?php endforeach ?>
				</select>
			</div>

			<div style="color: red;float: right;font-size: 13px;">(*Cevapları görmek için sorulara tıklayınız)</div>

			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th>ID</th>
						<th>Soru</th>
						<th>Anket</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($questions as $question): ?>
						<tr>
							<td>
								<?=$question['qid']?>
							</td>
							<td>
								<a href="<?=base_url()?>manage/question_manage/question_answer/<?=$question['qid']?>" style="text-decoration: none;color: #000;font-weight: 500;">
									<?=$question['qquestion']?>
								</a>
							</td>
							<td id="survey_title" data-title="<?=$question['sid']?>" ><?=$question['stitle']?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>