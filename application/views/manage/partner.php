<?php include "head.php";?>

	<?php include "nav.php";?>	
	<script type="text/javascript">
		$(document).ready(function(){
			Partner.init();

		});

	

	</script>
	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">
		<div class="fade-page" id="new_proc">
			<form role="form" id="forms">
				<input type="hidden" name="p_id" />

				<div class="form-group">
					<label>Bayi</label>
					<input type="text" class="form-control" name="p_name" >
				</div>

				<div class="form-group">
					<label>Vergi Dairesi</label>
					<input type="text" class="form-control" name="p_tax_office" >
				</div>

				<div class="form-group">
					<label>Vergi Numarası</label>
					<input type="text" class="form-control" name="p_tax_number" >
				</div>

				<div class="form-group">
					<label>Firma Adresi</label>
					<input type="text" class="form-control" name="p_address" >
				</div>

				<div class="form-group">
					<label>Firma Telefonu</label>
					<input type="text" class="form-control" name="p_tel" >
				</div>

				<div class="form-group">
					<label>Firma E-posta</label>
					<input type="text" class="form-control" name="p_email" >
				</div>

				<a class="btn btn-default btn-sm new" id="inpt-btn">Ekle</a>
				<a class="btn btn-danger btn-sm" href="<?php echo base_url();?>manage/partner_manage">Vazgeç</a>
			</form>
		</div>
		<div class="fade-page" style="display:block;">

			

			<a class="btn btn-sm btn-warning input-buttons" data-page-id="new_proc">Yeni Kayıt</a>
			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th width="120">ID</th>
						<th>Bayi</th>
						<th>Vergi Dairesi</th>
						<th>Vergi Numarası</th>
						<th style="width:120px;"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>