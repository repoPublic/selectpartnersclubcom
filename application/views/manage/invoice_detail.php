<?php include "head.php";?>

	<?php include "nav.php";?>	
	<script type="text/javascript">
		$(document).ready(function(){
			InvoiceDetail.init();

		});

	

	</script>
	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">
		<div class="fade-page" id="new_proc">
			<form role="form" id="forms">
				<input type="hidden" name="id" />

				<!-- <div class="form-group">
					<label>Başlık</label>
					<input type="text" class="form-control" name="title" >
				</div>

				<div class="form-group">
					<label>Mobil Görsel</label><br/>
					<input type="hidden" name="img_mobile">
					<div style="float:left;width:100%;height:1px;"></div>
					<input type="button" id="upload_file1">
	            	<ul id="gorseller1" style="float:left;width:100%;">
	            	</ul>
				</div>

				<div class="form-group">
					<label>Link</label>
					<input type="text" class="form-control" name="link" >
				</div>

				<div class="form-group">
					<label>Sıralama</label>
					<input type="text" class="form-control" name="sort" >
				</div> -->


				<a class="btn btn-default btn-sm new" id="inpt-btn">Ekle</a>
				<a class="btn btn-danger btn-sm" href="<?php echo base_url();?>manage/invoice_manage">Vazgeç</a>
			</form>
		</div>
		<div class="fade-page" style="display:block;">

			

			<!-- <a class="btn btn-sm btn-warning input-buttons" data-page-id="new_proc">Yeni Kayıt</a> -->
			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th>Adet</th>
						<th>Vergi Numarası</th>
						<th>Fatura No</th>
						<th>Fatura Tarihi</th>
						<th>Marka</th>
						<th>Model</th>
						<th>Ürün No</th>
						<th>Bayi</th>
						<th>Kazanılan Puan</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>