<?php include "head.php";?>

	<?php include "nav.php";?>	

	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">
		
		<div class="fade-page" style="display:block;">

			<a class="btn btn-sm btn-warning input-buttons" href="<?php echo base_url();?>manage/report_manage/to_excel/<?=$table?>/data/<?=$startDate?>/<?=$endDate?>/<?=$partner?>/<?=$user?>">Excel Export</a>

			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th>Kullanıcı</th>
						<th>Bayi</th>
						<th>Tarih</th>
						<th>Açıklama</th>
						<th>IP Adres</th>
					</tr>
					
					<?php foreach ($reports as $report): ?>
						
						<tr>
							<td><?php echo $report['name']; ?></td>
							<td><?php echo $report['Pname']; ?></td>
							<td><?php echo $report['created_date']; ?></td>
							<td><?php echo $report['comment']; ?></td>
							<td><?php echo $report['ip_address']; ?></td>
						</tr>

					<?php endforeach ?>

				</thead>
			</table>
		</div>
	</div>