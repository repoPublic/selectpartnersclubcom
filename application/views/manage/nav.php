<style type="text/css">
    .navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:hover, .navbar-default .navbar-nav>.open>a:focus {background-color: rgba(0, 0, 0, 0.3);}
</style>
<nav class="navbar navbar-default " style="background-image: linear-gradient(to bottom,#d44c48 0,rgb(239, 24, 23) 100%);">  
    <div class="navbar-header">          
        <a class="navbar-brand" style="padding: 4px;" href="<?php echo base_url();?>manage/partner_manage">
            <img src="http://selectpartnersclub.com/assets/images/logo-white.png" style="width: 90px;margin-left: 2px;margin-right: 20px;" class="img-responsive">
        </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">

		<ul class="nav navbar-nav">
            <li><a href="<?php echo base_url();?>manage/partner_manage" style="color: #fff">Bayiler</a></li>
            <li><a href="<?php echo base_url();?>manage/user_manage" style="color: #fff">Kullanıcılar</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #fff">Fatura <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>manage/invoice_manage" >Fatura Detay</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url();?>manage/invoice_manage/fatura_import_form" >Faturaları Yükle</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url();?>manage/invoice_manage/faturalar_import_form" >Faturaların Detayını Yükle</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url();?>manage/invoice_manage/campaign_point_index" >Kampanya Puanlarını Yükle</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #fff">Hediyeler <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>manage/gift_manage/get_requests">Hediye Talepleri</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url();?>manage/gift_manage">Hediye Kataloğu</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #fff">İçerikler <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>manage/slider_manage">Slider</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url();?>manage/video_manage">Video</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url();?>manage/campaignbanner_manage">Kampanyalar</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #fff">Anket <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>manage/survey_manage">Anket Ekle</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url();?>manage/question_manage/main">Soru ve Cevap</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #fff">Raporlar <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url();?>manage/report_manage">Tüm hareketler</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url();?>manage/report_manage/most_clicked_index">En çok yapılan hareketler</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url();?>manage/report_manage/total_points_index">Bayi Puanları</a></li>
              </ul>
            </li>
            <li><a href="<?php echo base_url();?>manage/movement_manage" style="color: #fff">Extra Puan</a></li>
            <li><a href="<?php echo base_url();?>manage/contact_manage" style="color: #fff">İletişim</a></li>
		</ul>

    <a href="<?php echo base_url();?>manage/login/logout" style="margin-top:10px;" class="btn btn-default btn-sm pull-right">
          <span class="glyphicon glyphicon-log-out"></span> Çıkış
    </a>

</nav>