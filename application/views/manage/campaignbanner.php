<?php include "head.php";?>

	<?php include "nav.php";?>	
	<script type="text/javascript">
		$(document).ready(function(){
			Campaign_Banner.init();

			var areas = Array('detail');
			$.each(areas, function (i, area) {
				CKEDITOR.replace(area,
		    		{			   
						filebrowserBrowseUrl : '<?=base_url()?>editor/ckfinder/ckfinder.html',
						filebrowserImageBrowseUrl : '<?=base_url()?>editor/ckfinder/ckfinder.html?Type=Images',
						filebrowserFlashBrowseUrl : '<?=base_url()?>editor/ckfinder/ckfinder.html?Type=Flash',
						filebrowserUploadUrl : '<?=base_url()?>editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
						filebrowserImageUploadUrl : '<?=base_url()?>editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
						filebrowserFlashUploadUrl : '<?=base_url()?>editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
				    });
			 });

		});

	

	</script>
	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">
		<div class="fade-page" id="new_proc">
			<form role="form" id="forms">
				<input type="hidden" name="id" />

				<div class="form-group">
					<label>Başlık</label>
					<input type="text" class="form-control" name="title" >
				</div>

				<div class="form-group">
					<label>Görsel</label><br/>
					<input type="hidden" name="img">
					<div style="float:left;width:100%;height:1px;"></div>
					<input type="button" id="upload_file">
	            	<ul id="gorseller" style="float:left;width:100%;">
	            	</ul>
				</div>

				<div class="form-group">
			    	<label>Detay</label>
			    	<textarea name="detail" id="detail" class="form-control ckeditor" rows="6"></textarea>
			    </div>

				<div class="form-group">
					<label>Sıralama</label>
					<input type="text" class="form-control" name="sort" >
				</div>


				<a class="btn btn-default btn-sm new" id="inpt-btn">Ekle</a>
				<a class="btn btn-danger btn-sm" href="<?php echo base_url();?>manage/campaignbanner_manage">Vazgeç</a>
			</form>
		</div>
		<div class="fade-page" style="display:block;">

			

			<a class="btn btn-sm btn-warning input-buttons" data-page-id="new_proc">Yeni Kayıt</a>
			<!-- <a class="btn btn-success btn-sm" href="<?php echo base_url();?>manage/blogCat_manage">Kategori Ekle</a> -->
			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th>ID</th>
						<th>Gorsel</th>
						<th>Başlık</th>
						<th>Sıralama</th>
						<th style="width:120px;"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>