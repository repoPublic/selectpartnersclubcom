<?php include "head.php";?>

	<?php include "nav.php";?>	

	<script type="text/javascript">
		$(document).ready(function(){

			$("#filtrele").change(function(){
				var val = $(this).val();
				$("#list tbody tr").each(function(ind, elem){
					
					if(val == "-1"){
						$(elem).show();
					}else{
						if($(elem).find("#survey_title").attr('data-title') != val){
							$(elem).hide();
						}else{
							$(elem).show();
						}
					}
				});
			});

		});
	
	</script>

	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">

		<a class="btn btn-sm btn-warning input-buttons" href="<?=base_url()?>manage/question_manage/main">Sorulara Geri Dön</a>
		<div style="margin-top: 20px;">
				<select id="filtrele">
					<option value="-1">Anket Seçiniz</option>
					<?php foreach ($surveys as $survey): ?>
						<option value="<?=$survey['id']?>"><?=$survey['title']?></option>
					<?php endforeach ?>
				</select>
			</div>
		<div class="fade-page" style="display:block;">

			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th>Cevap</th>
						<th>Kullanıcı</th>
						<th>Anket</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($answers as $answer): ?>
						<tr>
							<td>
								<?=$answer['answer']?>
							</td>
							<td>
								<?=$answer['username']?>
							</td>
							<td id="survey_title" data-title="<?=$answer['sid']?>" >
								<?=$answer['stitle']?>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>