<?php include "head.php";?>

	<?php include "nav.php";?>	

	<script type="text/javascript">
		$(document).ready(function(){
			var urlHash = window.location.hash;
			if (urlHash == "#error") {
				alert("Başlangıç veya Bitiş tarihi girmediniz. Lüften girip tekrar dener misiniz");
			}
			if (urlHash == "#error1") {
				alert("Sorgu yanlış. Lüften girip tekrar dener misiniz");
			}
		});
	</script>

	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">
		
		<div class="fade-page" style="display:block;">

			
			<form method="POST" action="<?php echo base_url();?>manage/report_manage/most_clicked_pages" role="form" id="date_form">
				<div class="col-md-4">
					<div class="form-group">
						<label>*Başlangıç Tarih(Format: 1111-11-11)</label>
						<input type="text" class="form-control" name="startDate" value="<?php echo date('Y-m-d'); ?>" >
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>*Bitiş Tarih (Format: 1111-11-11)</label>
						<input type="text" class="form-control" name="endDate" value="<?php echo date('Y-m-d'); ?>" >
					</div>
				</div>
				<div class="col-md-12"></div>
				<!-- <div class="col-md-3">
					<div class="form-group">
						<label>Kullanıcı</label>
						<input type="text" class="form-control" name="user" >
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Bayi</label>
						<input type="text" class="form-control" name="partner" >
					</div>
				</div> -->
				<div class="col-md-8">
					<div class="form-group">
						<input type="submit" value="Excel Export" class="btn btn-danger " style="margin-top: 26px;width: 100%;">
					</div>
				</div>
			</form>

			
		</div>
	</div>