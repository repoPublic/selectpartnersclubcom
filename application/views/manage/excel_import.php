<?php include "head.php";?>

	<?php include "nav.php";?>	

	<script type="text/javascript">
		$(function() {

		  // We can attach the `fileselect` event to all file inputs on the page
		  $(document).on('change', ':file', function() {
		    var input = $(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    input.trigger('fileselect', [numFiles, label]);
		  });

		  // We can watch for our custom `fileselect` event like this
		  $(document).ready( function() {
		      $(':file').on('fileselect', function(event, numFiles, label) {

		          var input = $(this).parents('.input-group').find(':text'),
		              log = numFiles > 1 ? numFiles + ' files selected' : label;

		          if( input.length ) {
		              input.val(log);
		          } else {
		              if( log ) alert(log);
		          }

		      });
		  });
		  
		});
	</script>

	<div class="container-fluid" style="margin-top:40px;">
		
		<div class="fade-page" style="display:block;">

			<form  action="<?php echo base_url(); ?>manage/invoice_manage/excel_import" method="POST" enctype="multipart/form-data">

			<div class="col-lg-6 col-sm-6 col-12">

	            <h4>Faturaları Yükle</h4>
	            <div class="input-group">
	                <label class="input-group-btn">
	                    <span class="btn btn-primary">
	                        Browse&hellip; <input type="file" name="file"  style="display: none;" multiple>
	                    </span>
	                </label>
	                <input type="text" class="form-control" readonly>
	            </div>

	            <div class="input-group" style="margin-top: 20px;width: 100%;">
	            	<input type= "submit" class="btn btn-success pull-right" value ="YÜKLE" >
	            </div>

	        </div>
			 
			 
			</form>

		</div>
	</div>