<?php include "head.php";?>

	<?php include "nav.php";?>	
	<script type="text/javascript">
		$(document).ready(function(){

			$(".hediye_durum").on('change', function(){
				var val = $(this).val();
				var talep_id = $(this).attr("talep_id");
				$.post("<?=base_url()?>manage/gift_manage/change_status", {
					talep_id : talep_id,
					val : val
				}, function(d){
					if(d > 0){
						console.log("Durum değiştirildi");
					}else{
						alert("Bir sorun oluştu");
					}
				});
			});

			$("#filtrele").change(function(){
				var val = $(this).val();
				$("#list tbody tr").each(function(ind, elem){
					
					if(val == "-1"){
						$(elem).show();
					}else{
						if($(elem).find(".hediye_durum").val() != val){
							$(elem).hide();
						}else{
							$(elem).show();
						}
					}
				});
			});

		});

	

	</script>
	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">

		<div style="width:100%; float:left;">
			<select id="filtrele">
				<option value="-1">Tüm Talepler</option>
				<option value="0">Pasif</option>
				<option value="1">Aktif</option>
			</select>
		</div>

		<div class="fade-page" style="display:block;">

			

			<!-- <a class="btn btn-sm btn-warning input-buttons" data-page-id="new_proc">Yeni Kayıt</a> -->
			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th>Kullanıcı</th>
						<th>Bayi</th>
						<th>İstenen hediye</th>
						<th>Tarih</th>
						<th>Puanı</th>
						<th>TL</th>
						<th>Durumu</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($talepler as $talep): ?>
						<tr>
							<td><?=$talep['Uname']?></td>
							<td><?=$talep['Pname']?></td>
							<td><?=$talep['gName']?></td>
							<td><?=$talep['mygDate']?></td>
							<td><?=$talep['gPoint']?></td>
							<td><?=$talep['gPrice']?></td>
							<td>
								<select class="hediye_durum" talep_id="<?=$talep['myId']?>">
									<option value="0" <?php echo $talep['gActive'] == 0 ? 'selected' : '';  ?> >Pasif</option>
									<option value="1" <?php echo $talep['gActive'] == 1 ? 'selected' : '';  ?> >Aktif</option>
								</select>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>