<?php include "head.php";?>

	<?php include "nav.php";?>	
	<script type="text/javascript">
		$(document).ready(function(){
			Survey.init();

		});

	

	</script>
	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">
		<div class="fade-page" id="new_proc">
			<form role="form" id="forms">
				<input type="hidden" name="id" />
				
				<div class="form-group">
					<label>Başlık</label>
					<input type="text" class="form-control" name="title" >
				</div>

				<div class="form-group">
					<label>Puan</label>
					<input type="text" class="form-control" name="survey_point" >
				</div>

				<div class="form-group">
					<label>Para</label>
					<input type="text" class="form-control" name="survey_price" >
				</div>

				<div class="form-group">
					<label>Yayınla (Aktif:1 , Pasif:0)</label>
					<input type="text" class="form-control" name="active" >
				</div>

				<a class="btn btn-default btn-sm new" id="inpt-btn">Ekle</a>
				<a class="btn btn-danger btn-sm" href="<?php echo base_url();?>manage/survey_manage">Vazgeç</a>
			</form>
		</div>
		<div class="fade-page" style="display:block;">

			

			<a class="btn btn-sm btn-warning input-buttons" data-page-id="new_proc">Yeni Kayıt</a>
			<a class="btn btn-sm btn-success input-buttons" href="<?php echo base_url();?>manage/question_manage">Ankete Soru Ekle</a>
			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th>ID</th>
						<th>Başlık</th>
						<th>Puan</th>
						<th>Para</th>
						<th>Durumu (Aktif:1 , Pasif:0)</th>
						<th style="width:120px;"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>