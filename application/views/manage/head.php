<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<title>CMS</title>
			<link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>/fuji_favicon.ico" />
			<link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/admin/admin.css">
			<link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap-theme.min.css">
			<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/datatables/datatables.css">
			<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/datatables/media/css/TableTools.css">
			<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugin/tagsinput/bootstrap-tagsinput.css">
			
			<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.js"></script>
			<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
			<script type="text/javascript" src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
			
			<script type="text/javascript">var base_url = '<?php echo base_url();?>';</script>
			
			<script type="text/javascript" src="<?php echo base_url();?>assets/admin/datatables/jquery.dataTables.js"></script>
			<script type="text/javascript" src="<?php echo base_url();?>assets/admin/datatables/jquery.dataTables.fnReloadAjax.js"></script>
			<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/plugin/tagsinput/bootstrap-tagsinput.js"></script>
			<script type="text/javascript" src="<?php echo base_url();?>assets/plugin/tagsinput/bootstrap-tagsinput-angular.js"></script> -->

			<script type="text/javascript" src="<?php echo base_url();?>assets/admin/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
			<script type="text/javascript" src="<?php echo base_url();?>assets/admin/uploadify/swfobject.js"></script>



			<script type="text/javascript" src="<?=base_url()?>assets/admin/main.js"></script>
			<script src="<?php echo base_url();?>editor/ckeditor/ckeditor.js"></script>
			<!-- <script src='<?php echo base_url();?>tinymce/js/tinymce/tinymce.min.js'></script> -->

		</head>
		