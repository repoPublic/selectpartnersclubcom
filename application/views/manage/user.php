<?php include "head.php";?>

	<?php include "nav.php";?>	
	<script type="text/javascript">
		$(document).ready(function(){
			User.init();

			$('#inpt-btn').click(function(){
				if ($('select option:selected').text()=='Seçiniz') {
					alert('Lütfen Menü Seçiniz !');
					$('#sec').focus();
					return false;
				}
			});

		});

	

	</script>
	<style type="text/css">
		.content-fade,.buttons-box,.all-done {display: none;}
		.tagator, .inputTagator{
			display:inline-table;
			width: 100% !important;
		}
	</style>
	<div class="container-fluid" style="margin-top:40px;">
		<div class="fade-page" id="new_proc">
			<form role="form" id="forms">
				<input type="hidden" name="id" />

				<div class="form-group">
					<label>Ad Soyad</label>
					<input type="text" class="form-control" name="name" >
				</div>

				<div class="form-group">
					<label>Bayi</label>
					<select class="form-control" name="partner_id">
						<option disabled selected id="sec">Seçiniz</option>
						<?php foreach($partner as $v){ 
							echo '<option value="'.$v['p_id'].'" '.$select.' id="partner_'.$v['p_id'].'">'.$v['p_name'].'</option>'; } 
						?>
					</select>
				</div>

				<div class="form-group">
					<label>Posizyon</label>
					<input type="text" class="form-control" name="position" >
				</div>

				<div class="form-group">
					<label>Email</label>
					<input type="text" class="form-control" name="email" >
				</div>

				<div class="form-group">
					<label>Parola</label>
					<input type="text" class="form-control" name="password" >
				</div>

				<div class="form-group">
					<label>Fotoğraf</label><br/>
					<input type="hidden" name="picture">
					<div style="float:left;width:100%;height:1px;"></div>
					<input type="button" id="upload_file">
	            	<ul id="gorseller" style="float:left;width:100%;">
	            	</ul>
				</div>

				<div class="form-group">
					<label>Telefon Numarası</label>
					<input type="text" class="form-control" name="gsm" >
				</div>

				<div class="form-group">
					<label>Şehir</label>
					<input type="text" class="form-control" name="city" >
				</div>

				<div class="form-group">
					<label>Durumu( 1 veya 0)</label>
					<input type="text" class="form-control" name="active" >
				</div>


				<a class="btn btn-default btn-sm new" id="inpt-btn">Ekle</a>
				<a class="btn btn-danger btn-sm" href="<?php echo base_url();?>manage/user_manage">Vazgeç</a>
			</form>
		</div>
		<div class="fade-page" style="display:block;">

			

			<a class="btn btn-sm btn-warning input-buttons" data-page-id="new_proc">Yeni Kayıt</a>
			<table  class="table table-striped table-hover" id="list">
				<thead>
					<tr>
						<th width="120">ID</th>
						<th>Fotoğraf</th>
						<th>Ad Soyad</th>
						<th>Email</th>
						<th>Posizyon</th>
						<th>Durumu</th>
						<th style="width:120px;"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>