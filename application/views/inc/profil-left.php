<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="profil-box">
        <div class="profil-picture" style="background-image: url('<?php echo base_url();?>uploaded_files/<?php echo $profil['picture']; ?>');">
            <div class="pp-mask"></div>
            <form name="pChangeForm" method="POST" action="/profil/upload_picture/<?php echo $profil['id']; ?>" enctype="multipart/form-data" >
                <input type="file" name="picture" class="picture-upload" onchange="pictureChooseFile();">
                <input type="submit" id="pic_update" style="display: none;" />
            </form>
            <i class="fa fa-camera fa-2x change-icon" aria-hidden="true"></i>
            <div class="picture-change-bg"></div>
        </div>

        <div class="profil-title"><?php echo $profil['name']; ?></div>
        <span class="profil-border"></span>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mb-hide">
                <?php if ($profil['p_tel']): ?><div class="profil-numberRow"><?php echo $profil['p_tel']; ?></div><?php endif ?>
                <div class="profil-numberRow gsm"><?php echo $profil['gsm']; ?></div>
                <a href="mailto:<?php echo $profil['email']; ?>" target="_blank" class="profil-numberRow mail"><?php echo $profil['email']; ?></a>        
            </div>
            <div class="col-md-12 hidden-sm hidden-xs ">
                <a href="<?php echo base_url();?>logout" class="profil-signout col-sm-12 col-xs-12">ÇIKIŞ YAP</a>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    function pictureChooseFile(){
        $('#pic_update').click();
    }
</script>