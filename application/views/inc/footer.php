<div class="container">


    <div id="footer">
        <div class="row">
            <div class="col-md-3 col-sm-2 col-xs-12">
                <div class="copyright">©2016 Fujitsu Tüm hakları saklıdır.</div>
            </div>
            <div class="col-md-5 col-sm-8 col-xs-12">
                <div class="footer-center">
                    <a href="">Kullanım Koşulları</a>
                    <span></span>
                    <a href="">Gizlilik</a>
                    <span></span>
                    <a href="">Teknik Servis</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-2 col-xs-12 footer-social">

                <a href="<?php echo base_url();?>iletisim" class="contact-btn">İletişim</a>
                
                <a href="https://twitter.com/fujitsuturkey" target="_blank"><img src="<?php echo base_url();?>assets/images/footer-tw.png"></a>

                <a href="https://www.facebook.com/FujitsuTurkey/?fref=ts" target="_blank"><img src="<?php echo base_url();?>assets/images/footer-fb.png"></a>

                <a href="https://www.youtube.com/channel/UCyFNVbssYMIMMKRugTngAaQ" target="_blank" style="color: #fff;font-size: 15px;"><i class="fa fa-youtube-play"></i></a>

                <a href="https://www.linkedin.com/company/fujitsu-turkey" target="_blank"><img src="<?php echo base_url();?>assets/images/footer-in.png"></a>

            </div>
        </div>
    </div>
</div>


<div id="preloader">
            
    <div class="sk-cube-grid">
      <div class="sk-cube sk-cube1"></div>
      <div class="sk-cube sk-cube2"></div>
      <div class="sk-cube sk-cube3"></div>
      <div class="sk-cube sk-cube4"></div>
      <div class="sk-cube sk-cube5"></div>
      <div class="sk-cube sk-cube6"></div>
      <div class="sk-cube sk-cube7"></div>
      <div class="sk-cube sk-cube8"></div>
      <div class="sk-cube sk-cube9"></div>
    </div>

</div>

