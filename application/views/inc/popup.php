<script type="text/javascript">
    function validateForm() {
        var q1 = $('#q1').val();
        var q2 = $('#q2').val();
        var q3 = $('#q3').val();
        var q4 = $('#q4').val();
        var q5 = $('#q5').val();
        
        if (q1 == null || q1 == "" || q2 == null || q2 == "" || q3 == null || q3 == "" || q4 == null || q4 == "") {
            alert("Lütfen Tüm Soruları Cevaplayınız");
            return false;
        }
        else{
            alert("Anket başarı bir şekilde kaydedildi. Teşekkürler");
        }
    }
</script>

<div class="content" style="position: relative;">
    <a class="popup-close"><i class="fa fa-times" aria-hidden="true"></i></a>
    
    <div class="popup-area">
    
        <div class="form-signin">
            <div class="loginLogo"><img src="<?php echo base_url();?>assets/images/white-logo.png"  class="img-responsive" style="display: inline-block;"/></div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="login-popupBox">

                    <form action="<?php echo base_url();?>home/answer_add/" method="post" onsubmit="return validateForm()">
                        <?php $i=1; ?>
                        <?php foreach ($questions as $q): ?>
                            <div class="popup-inputRow">
                                    
                                <div class="col-md-4 col-sm-3 col-xs-12"><div class="popup-inputTitle"><?php echo $q['question']; ?></div></div>

                                <div class="col-md-8 col-sm-9 col-xs-12"><input type="text" id="q<?php echo $i; ?>" class="popup-input" name="<?php echo "a-".$q['qid']; ?>"></div>
                                
                            </div>
                            <?php $i++; ?>
                        <?php endforeach ?>

                        

                            <div class="popup-inputRow" style="margin-top: 35px;">

                                <div class="col-md-4 col-sm-3 col-xs-12"></div>

                                <div class="col-md-8 col-sm-9 col-xs-12"><input type="submit" class="popup-submit" value="GÖNDER"></div>
                                
                            </div>
                    </form>

                </div>
            </div>
        </div>

    </div>

</div>