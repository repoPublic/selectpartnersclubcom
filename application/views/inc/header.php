<header>

    <div class="menu-icon">
        <div id="nav-icon3">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div class="hm-menu-bg"></div>
    <div class="hm-menu">

        <div class="menu">
            <a id="anasayfa-mb active" href="<?php echo base_url();?>" >ANASAYFA</a>
            <?php if(isset($_SESSION['id'])){ ?>
            <a id="egitim-mb" href="<?php echo base_url();?>egitim">EĞİTİM</a>
            <a id="hediyeler-mb" href="<?php echo base_url();?>hediyeler">HEDİYELER</a>
            <a id="kampanyalar-mb" href="<?php echo base_url();?>kampanyalar">KAMPANYALAR</a>
            <a id="branding-mb" href="<?php echo base_url();?>branding">PAZARLAMA DÖKÜMANLARI</a>
            <?php } ?>
            <a id="blog-mb" href="<?php echo base_url();?>blog">BLOG</a>
            <?php if(!isset($_SESSION['id'])){ ?>
            <a id="katilim-mb" href="<?php echo base_url();?>katilim">KATILIM FORMU</a>
            <a href="<?php echo base_url();?>login" class="newSignButton">GİRİŞ YAP <i class="fa fa-sign-in"></i></a>
            <?php }else{ ?>
            <a href="<?php echo base_url();?>logout" class="newSignButton">ÇIKIŞ YAP <i class="fa fa-sign-in"></i></a>
            <?php } ?>
        </div>

    </div>

</header>

<div id="header">
    <div class="top" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-8">
                    <?php if(isset($_SESSION['id'])){ ?>
                    <ul class="nav nav-pills nav-justified">
                        <li class="active"><a href="<?php echo base_url();?>profil"><span>PROFİLİM</span></a></li>
                        <li><a href="<?php echo base_url();?>profil/satislarim"><span>SATIŞLARIM</span></a></li>
                        <li><a href="<?php echo base_url();?>profil/puanekstresi"><span>PUAN EKSTRESİ</span></a></li>
                        <li><a href="<?php echo base_url();?>profil/hediyelerim"><span>HEDİYELERİM</span></a></li>
                    </ul>
                    <?php }else{ echo '&nbsp;'; } ?>
                </div>
                <div class="col-md-2">
                    <?php if(!isset($_SESSION['id'])){ ?>
                    <a href="<?php echo base_url();?>login" class="signBtn pull-right">Giriş Yap <i class="fa fa-angle-down"></i></a>
                    <?php }else{ ?>
                    <a href="<?php echo base_url();?>logout" class="signBtn pull-right">Çıkış Yap <i class="fa fa-angle-down"></i></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container"><div class="top" style="height: 5px;padding: 0 0;">&nbsp;</div></div>
    <div class="center">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <a href="<?php echo base_url();?>" class="logo"><img src="<?php echo base_url();?>assets/images/logo.png" class="img-responsive" /> </a>
                </div>
                <div class="col-md-10">
                    <ul class="nav nav-pills nav-justified">
                        <?php if(isset($_SESSION['id'])){ ?>
                        <li id="egitim"><a href="<?php echo base_url();?>egitim">Eğitim</a></li>
                        <li id="hediyeler"><a href="<?php echo base_url();?>hediyeler">Hediyeler</a></li>
                        <li id="kampanyalar"><a href="<?php echo base_url();?>kampanyalar">Kampanyalar</a></li>
                        <li id="branding"><a href="<?php echo base_url();?>branding">Pazarlama Dokümanları</a></li>
                        <?php } ?>
                        <li id="blog"><a href="<?php echo base_url();?>blog">Blog</a></li>
                        <li id="iletisim"><a href="<?php echo base_url();?>iletisim">İletişim</a></li>
                        <?php if(!isset($_SESSION['id'])){ ?>
                        <li id="katilim"><a href="<?php echo base_url();?>katilim">Katılım Formu</a></li>
                        <li><a href="<?php echo base_url();?>login" class="newSignButton">Giriş Yap <i class="fa fa-sign-in"></i></a></li>
                        <?php }else{ ?>
                        <li><a href="<?php echo base_url();?>logout" class="newSignButton">Çıkış Yap <i class="fa fa-sign-in"></i></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="fixed-anasayfa-btn">
    <a href="<?php echo base_url();?>"><i class="fa fa-home fa-2x" aria-hidden="true"></i></a>
</div>