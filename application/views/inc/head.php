<head>
    <meta charset="utf-8">
    <title>Fujitsu Select Partners Club</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>/fuji_favicon.ico" />
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib/owlcarousel/css/owl-carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">


    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/web.js"></script>
	
    

</head>
