<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<script type="text/javascript">
    function validateForm() {
        var ad = document.forms["katilimForm"]["Ad_Soyad"].value;
        var eposta = document.forms["katilimForm"]["E_posta"].value;
        var v_daire = document.forms["katilimForm"]["Vergi_Dairesi"].value;
        var v_no = document.forms["katilimForm"]["Vergi_Numarasi"].value;
        var f_adres = document.forms["katilimForm"]["Firma_Adresi"].value;
        var f_gorev = document.forms["katilimForm"]["Firmadaki_Gorevi"].value;
        var f_tel = document.forms["katilimForm"]["Firma_Telefonu"].value;
        var f_email = document.forms["katilimForm"]["Firma_E_posta"].value;
        var tel = document.forms["katilimForm"]["Kullanici_Telefonu"].value;
        // if (ad == null || ad == "") {
        //     alert("Lütfen Ad Soyad Giriniz");
        //     return false;
        // }
        // if (eposta == null || eposta == "") {
        //     alert("Lütfen E-mail Giriniz");
        //     return false;
        // }
        // if (tel == null || tel == "") {
        //     alert("Lütfen Telefon Giriniz");
        //     return false;
        // }
        // if (mesaj == null || mesaj == "") {
        //     alert("Lütfen Mesaj Giriniz");
        //     return false;
        // }
    }
</script>
<body>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="page-title">KATILIM FORMU</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">

                    <div class="katilim-left">
                        
                        Select Partners Club'ın ayrıcalıklı avantajlarından faydalanmak için siz de bize katılın!

                    </div>

                </div>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    
                    <div class="katilim-right">
                        <form name="katilimForm" method="POST" action="<?php echo base_url(); ?>katilim/add" onsubmit="return validateForm()" >
                            
                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Ad Soyad</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Ad_Soyad"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">E-posta</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="E_posta"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Kullanıcı Telefonu</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Kullanici_Telefonu"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Firma</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12">

                                    <select name="Firma" id="firma" class="katilim-input" style="height: 41px;">
                                        <option></option>
                                        <?php foreach ($partners as $partner): ?>
                                            <option value="<?php echo $partner['p_id']; ?>"><?php echo $partner['p_name']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Pozisyon</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="position"></div>
                                
                            </div>

                            <!-- <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Vergi Dairesi</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Vergi_Dairesi"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Vergi Numarası</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Vergi_Numarasi"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Firma Adresi</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Firma_Adresi"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Firmadaki Görevi</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Firmadaki_Gorevi"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Firma Telefonu</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Firma_Telefonu"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Firma E-posta</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="Firma_E_posta"></div>
                                
                            </div> -->

                            <div class="katilim-inputRow" style="margin-top: 35px;">

                                <div class="col-md-3 col-sm-3 col-xs-12"></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="submit" class="katilim-submit" value="Katıl"></div>
                                
                            </div>

                        </form>
                    </div>                    

                </div>
            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>
</body>
</html>