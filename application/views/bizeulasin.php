<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<script type="text/javascript">

    $(document).ready(function(){
        var hashUrl = window.location.hash;
        if (hashUrl == "#success") {
            $("#contact-bttn").click();
        }
    });

    function validateForm() {
        var ad = document.forms["contactForm"]["adsoyad"].value;
        var eposta = document.forms["contactForm"]["eposta"].value;
        var tel = document.forms["contactForm"]["tel"].value;
        var mesaj = document.forms["contactForm"]["mesaj"].value;
        if (ad == null || ad == "") {
            alert("Lütfen Ad Soyad Giriniz");
            return false;
        }
        if (eposta == null || eposta == "") {
            alert("Lütfen E-mail Giriniz");
            return false;
        }
        if (tel == null || tel == "") {
            alert("Lütfen Telefon Giriniz");
            return false;
        }
        if (mesaj == null || mesaj == "") {
            alert("Lütfen Mesaj Giriniz");
            return false;
        }
        else{
            $("#preloader").css("display","block !important");
        }
    }
</script>
<body>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont" style="margin-bottom: 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="page-title">İLETİŞİM</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">

                    <div class="katilim-left" >
                        
                        <img src="<?php echo base_url(); ?>assets/images/bizeulasin.png" class="img-responsive center-block">

                    </div>

                    <div class="adress">
                        <div class="title"><b>İstanbul Ofis</b></div>
                        <table class="table">
                            <tr>
                                <td><b>Adres:</b> FSM Mah. Poligon Cad. No:8A Buyaka2 Sitesi Kule1 Kat:12 34771 Ümraniye / İstanbul</td>
                            </tr>
                            <tr>
                                <td><b>Tel:</b> (216) 586-4000</td>
                            </tr>
                            <tr>
                                <td><b>Faks:</b> (216) 586-4105</td>
                            </tr>
                        </table>
                    </div>

                    <div class="adress">
                        <div class="title"><b>Ankara Ofis</b></div>
                        <table class="table">
                            <tr>
                                <td><b>Adres:</b> İlkbahar Mah. Turan Güneş Bulvarı 571 Cad. 613 Sok. No:3 Çankaya,06550 Ankara</td>
                            </tr>
                            <tr>
                                <td><b>Tel:</b> (312) 463 6400</td>
                            </tr>
                            <tr>
                                <td><b>Faks:</b> (312) 463 6483</td>
                            </tr>
                        </table>
                    </div>

                </div>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    
                    <div class="katilim-right">

                        <form name="contactForm" method="POST" action="iletisim/contact_add" enctype="multipart/form-data" onsubmit="return validateForm()" >
                            
                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Adı Soyadı</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="adsoyad"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">E-posta</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="email" class="katilim-input" name="eposta"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Telefon</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="text" class="katilim-input" name="tel"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Dosya Yükle</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="file" class="katilim-input" name="upload_ek"></div>
                                
                            </div>

                            <div class="katilim-inputRow">

                                <div class="col-md-3 col-sm-3 col-xs-12"><div class="katilim-inputTitle">Mesaj</div></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><textarea name="mesaj" class="katilim-input" rows="6"></textarea></div>
                                
                            </div>

                            
                            <div class="katilim-inputRow" style="margin-top: 25px;">

                                <div class="col-md-3 col-sm-3 col-xs-12"></div>

                                <div class="col-md-9 col-sm-9 col-xs-12"><input type="submit" class="katilim-submit" value="Gönder"></div>
                                
                            </div>

                        </form>

                        <div class="info" >
                            <p>Kontratlı müşterilerimiz için 7 gün / 24 saat aranabilecek iletişim numaraları;</p>
                            <table class="table">
                                <tr>
                                    <td>Telefon :</td>
                                    <td>
                                        <a href="tel:+0800-219-67-05">0800-219-67-05</a><br /><br />
                                        Cep telefonundan ulaşım:<br />
                                        <a href="tel:+0216-387-0017">0216 387 0017</a><br />
                                        <a href="tel:+0216-387-2036">0216 387 2036</a><br />
                                        <a href="tel:+0216-387-2037">0216 387 2037</a><br />
                                        <a href="tel:+0216-387-0193">0216 387 0193</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>E-mail adres :</td>
                                    <td><a href="mailto:Servicedesk_tr@ts.fujitsu.com">Servicedesk_tr@ts.fujitsu.com</a></td>
                                </tr>
                            </table><br />

                            <p>Standart garanti altında satılan Fujitsu ürünleri için 5 gün x 9 saat (mesai saatleri 08:00 ile 17:00 arası) aranabilecek iletişim numaraları ;</p>
                            <table class="table">
                                <tr>
                                    <td>Telefon :</td>
                                    <td><a href="tel:+444 0 372"></a>444 0 372</td>
                                </tr>
                                <tr>
                                    <td>E-mail adres :</td>
                                    <td><a href="mailto:helpdesk_tr@ts.fujitsu.com">helpdesk_tr@ts.fujitsu.com</a></td>
                                </tr>
                            </table>

                        </div>

                    </div>         



                </div>

                <div class="col-md-3"></div>
               

            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<button type="button" style="display:none" data-toggle="modal" id="contact-bttn" data-target="#myModal">
</button>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">İletişim Formu</h4>
      </div>
      <div class="modal-body">
         Form başarılı bir şekilde kaydedildi. En kısa zamanda geri dönüş olacaktır. Teşekkürler.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
        
      </div>
    </div>
  </div>
</div>

<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>
</body>
</html>