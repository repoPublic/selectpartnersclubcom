<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<script type="text/javascript">

    $(document).ready(function(){
        var hashUrl = window.location.hash;
        var pathName = window.location.pathname;
        if (hashUrl == "#kayit-basarili") {
            $("#home-bttn").click();
        }
        if (pathName == "/home/webinar") {
            $("#webinar-bttn").click();
        }
    });
</script>
<style type="text/css">
    .webinar .logo{
        width: 100%;
        position: relative;
        left: 50%;
        margin-left: -55px;
        margin-bottom: 30px;
        margin-top: 30px;
    }
    .webinar .logo img {
        width: 110px;
    }
    .webinar .detail{
        width: 80%;
        background: #ffffff;
        padding: 20px;
        position: relative;
        left: 50%;
        margin-left: -40%;
        margin-bottom: 50px;
    }
    .webinar .detail .title{
        color: #ec1d24;
        font-weight: bold;
        text-align: center;
        font-size: 16px;
        margin-bottom: 10px;
    }
    .webinar .detail .list{
        color: #000;
        font-size: 13px;
        line-height: 1.7;
        margin-bottom: 35px;
    }
    .webinar .detail .webinar-btn{
        position: relative;
        width: 100%;
        left: 50%;
        margin-left: -88px;
            margin-bottom: 18px;
    }
    .webinar .detail .webinar-btn a{
        text-decoration: none;
        background: #ec1d24;
        color: #fff;
        padding: 7px 35px;
        text-align: center;
        border-radius: 6px 6px 6px 6px;
        -moz-border-radius: 6px 6px 6px 6px;
        -webkit-border-radius: 6px 6px 6px 6px;
    }
</style>
<body>


<?php include "inc/header.php"; ?>
<?php if(isset($_SESSION['popup'])){ unset($_SESSION['popup']); ?>
<div class="popup-blkBg"></div>
<div class="popup-box login">
    <?php include "inc/popup.php"; ?>
</div>
<?php } ?>

<?php if(isset($_SESSION['id'])){ ?>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <video id="video" width="100%" height="400" controls>
                          <source src="" type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="content">
    <div class="container">
        <div id="banner" style="margin-top: 6px">
            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <?php $i = 0; ?>
                    <?php foreach ($sliders as $slider): ?>
                        <?php if ($i == 0){ ?>
                            <li data-target="#carousel-example" data-slide-to="<?php echo $i; ?>" class="active"></li>
                        <?php }else{ ?>
                            <li data-target="#carousel-example" data-slide-to="<?php echo $i; ?>"></li>
                        <?php } $i++; ?>
                    <?php endforeach ?>
                </ol>

                <div class="carousel-inner">
                    <?php $i = 0; ?>
                    <?php foreach ($sliders as $slider): ?>
                        <?php if ($i == 0){ ?>
                            <div class="item active">
                                <a href="<?php echo $slider['link']; ?>">
                                    <img src="<?php echo base_url();?>uploaded_files/<?php echo $slider['img']; ?>" class="hidden-xs" />
                                    <img src="<?php echo base_url();?>uploaded_files/<?php echo $slider['img_mobile']; ?>" class="hidden-lg hidden-md hidden-sm" />
                                </a>
                            </div>
                        <?php }else{ ?>
                            <div class="item">
                                <a href="<?php echo $slider['link']; ?>">
                                    <img src="<?php echo base_url();?>uploaded_files/<?php echo $slider['img']; ?>" class="hidden-xs" />
                                    <img src="<?php echo base_url();?>uploaded_files/<?php echo $slider['img_mobile']; ?>" class="hidden-lg hidden-md hidden-sm" />
                                </a>
                            </div>
                        <?php } $i++; ?>
                        
                    <?php endforeach ?>
                </div>

                <a class="left carousel-control hidden-xs" href="#carousel-example" data-slide="prev" style="left: -60px">
                    <span class="fa fa-chevron-circle-left"></span>
                </a>
                <a class="right carousel-control hidden-xs" href="#carousel-example" data-slide="next" style="right: -30px">
                    <span class="fa fa-chevron-circle-right"></span>
                </a>
            </div>
        </div>
    </div>
    <div id="boxList">
        <div class="container">
            <div class="row">
                <?php if(isset($_SESSION['id'])){ ?>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="<?php echo base_url();?>profil" class="profil-links">
                        <img src="<?php echo base_url();?>assets/images/profilim_sb.png" class="img-responsive profil-passive" width="100%" />
                        <img src="<?php echo base_url();?>assets/images/profilim_r.png" class="img-responsive profil-active" width="100%" />
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="<?php echo base_url();?>profil/satislarim" class="profil-links">
                        <img src="<?php echo base_url();?>assets/images/satislarim_sb.png" class="img-responsive profil-passive" width="100%" />
                        <img src="<?php echo base_url();?>assets/images/satislarim_r.png" class="img-responsive profil-active" width="100%" />
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="<?php echo base_url();?>profil/puanekstresi" class="profil-links">
                        <img src="<?php echo base_url();?>assets/images/puanekstresi_sb.png" class="img-responsive profil-passive" width="100%" />
                        <img src="<?php echo base_url();?>assets/images/puanekstresi_r.png" class="img-responsive profil-active" width="100%" />
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <a href="<?php echo base_url();?>profil/hediyelerim" class="profil-links">
                        <img src="<?php echo base_url();?>assets/images/hediyelerim_sb.png" class="img-responsive profil-passive" width="100%" />
                        <img src="<?php echo base_url();?>assets/images/hediyelerim_r.png" class="img-responsive profil-active" width="100%" />
                    </a>
                </div>
                <?php }else{ ?>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <img src="<?php echo base_url();?>assets/images/4lu-banner1.png" class="img-responsive" width="100%" />
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <img src="<?php echo base_url();?>assets/images/4lu-banner2.png" class="img-responsive" width="100%" />
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <img src="<?php echo base_url();?>assets/images/4lu-banner3.png" class="img-responsive" width="100%" />
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <img src="<?php echo base_url();?>assets/images/4lu-banner4.png" class="img-responsive" width="100%" />
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php if(isset($_SESSION['id'])){ ?>
    <div class="container">
        <div id="owlCarousel">
            <div class="owl-carousel">

                <?php foreach ($videos as $video): ?>
                    <?php if ($video['link'] == null || $video['link'] == ""): ?>

                        <a href="<?php echo $video['video']; ?>" class="item videoListLink" id="v<?php echo $video['id']; ?>" partner_id="<?php echo $_SESSION['partner_id']; ?>" data-text="<?php echo $video['title']; ?>" style="position: relative;">
                            <img src="<?php echo base_url();?>uploaded_files/<?php echo $video['img']; ?>" width="350" class="img-responsive"/>
                            <span style="display: none;"></span>
                        </a>

                    <?php endif ?>
                <?php endforeach ?>


            </div>
        </div>
    </div>
    <div class="container">
        <div id="owlCarousel">
            <div class="owl-carousel kampanya-carousel">
                <a href="<?php echo base_url(); ?>kampanyalar" class="item">
                    <img src="<?php echo base_url();?>assets/images/kamp1.png" class="img-responsive"/>
                </a>
                <a href="<?php echo base_url(); ?>kampanyalar" class="item">
                    <img src="<?php echo base_url();?>assets/images/kamp2.png" class="img-responsive"/>
                </a>
                <a href="<?php echo base_url(); ?>kampanyalar" class="item">
                    <img src="<?php echo base_url();?>assets/images/kamp3.png" class="img-responsive"/>
                </a>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="container">
        <div id="platform">
            <div class="row">

                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="box">
                        <div class="title" style="background-color:#002c5c;"><i class="fa fa-comments"></i></div>
                        <div class="list-left blog">
                            <div class="row">

                            <?php $sayi = 0; ?>
                            <?php foreach ($blogs as $blog): ?>
                                <?php if ($sayi < 3){ ?>
                                    <?php if ($sayi < 1){ ?>
                                        <div class="col-md-7 col-sm-12 col-xs-12">
                                            <a href="<?php echo $blog['link']; ?>" target="_blank">
                                                <img src="<?php echo $blog['image']; ?>" style="margin-bottom: 5px;" class="img-responsive web-height1"/>
                                                <span class="text">
                                                    <?php echo substr($blog['title'],0,45) . " ..."; ?>
                                                </span>
                                            </a>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="col-md-5 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                            <a href="<?php echo $blog['link']; ?>" target="_blank">
                                                <img src="<?php echo $blog['image']; ?>" class="img-responsive web-height2"/>
                                                <span class="text-small">
                                                    <?php echo substr($blog['title'],0,45) . " ..."; ?>
                                                </span>
                                            </a>
                                        </div>
                                    <?php 
                                        }
                                        $sayi++;
                                    ?>

                                <?php } ?>

                            <?php endforeach ?>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="box">
                        <div class="title" style="background-color:#39a9e0;"><i class="fa fa-twitter"></i></div>
                        
                        <div class="list twitter" >
                            <a class="twitter-timeline" href="https://twitter.com/FujitsuTurkey">Tweets by FujitsuTurkey</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<button type="button" style="display:none" data-toggle="modal" id="home-bttn" data-target="#myModal">
</button>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Webinar Onay Mesajı</h4>
      </div>
      <div class="modal-body">
        Kaydınız başarılı bir şekilde gerçekleşmiştir. Teşekkürler.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
        
      </div>
    </div>
  </div>
</div>


<!-- Button trigger modal -->
<button type="button" style="display:none" data-toggle="modal" id="webinar-bttn" data-target="#myModal1">
</button>
<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content webinar" style="background-image: url('/assets/images/webinar/bg.png')">
        <div class="logo">
            <img src="<?php echo base_url(); ?>assets/images/webinar/logo.png">
        </div>
        <div class="detail">
            <div class="title">Webinar'a katılmak için aşağıdaki adımları izleyiniz; </div>
            <div class="list">
                1- Giriş butonuna tıklayınız.<br />
                2- Açılan sayfada Lync Web App eklentisini indirin ve yükleyin.<br />
                3- Yüklemeden sonra şuna tıklayın: Toplantıya katıl.<br />
                4-Lync Web App yeni bir pencerede açılacak ve webinara bağlanacaksınız.
            </div>
            <div class="webinar-btn">
                <a href="https://lync.ts.fujitsu.com/Meet/sinan.ozcan/2253BCN4" target="_blank">Webinar’a Katıl >></a>
            </div>
        </div>
    </div>
  </div>
</div>



<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>

<script type="text/javascript">
    $('.close, #myModal').click(function(){

        var video = document.getElementById('video');
        video.pause();
        
        clearInterval(countdown);

    });
</script>

</body>
</html>