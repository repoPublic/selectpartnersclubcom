<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<body>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <?php include 'inc/profil-left.php'; ?>

                <div class="col-md-9 col-sm-12 col-xs-12">
                    
                    <div class="profil-right">

                        <?php include 'inc/profil-top.php'; ?>
                        
                        <div class="profil-graph">
                            
                            <div class="graphBoxes-title">KULLANILABİLİR PUANLARINIZ</div>

                            <div class="profil-pointBox">
                                
                                
                                <div class="row">

                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <div class="profil-pointTitle">Primergy Puanınız</div>

                                        <div class="profil-point">
                                            <?php echo $primergy_toplam; ?>  
                                        </div>

                                        <div class="profil-pointTitle">Eternus Puanınız</div>

                                        <div class="profil-point"><?php echo $eternus_toplam; ?></div>

                                        <div class="profil-pointTitle">Hareket Puanınız</div>

                                        <div class="profil-point"><?php echo $movement_point; ?></div>

                                        <div class="profil-pointTitle">Kampanya Puanınız</div>

                                        <div class="profil-point"><?php echo $kampanya_puan; ?></div>

                                        <div class="profil-pointTitle">Kullanılan Puanınız</div>

                                        <div class="profil-point"><?php echo $kullanilan_puan; ?></div>

                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <div class="profil-totalPoint">
                                            Toplam Puanınız<br>
                                            <span><?php echo $toplam; ?></span>
                                        </div>

                                    </div>
                                    
                                </div>
                                
                            </div>

                        </div>

                        <div class="profil-salesBox">
                            
                            <div class="salesBox-title">SATIŞ PUANLARINIZ</div>

                            <div class="table-responsive border-none" >
                                <table width="100%" border="0" class="salesTable table">
                                    <thead>
                                        <tr>
                                            <th>Ürün Marka</th>
                                            <th>Ürün Model</th>
                                            <th>Ürün Adı</th>
                                            <th>Ürün Adeti</th>
                                            <th>Puan Çarpanı</th>
                                            <th>Kazanılan Puan</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php foreach ($sales as $sale): ?>
                                            <tr>
                                                <td><?php echo $sale['brand'] ?></td>
                                                <td><?php echo $sale['model'] ?></td>
                                                <td><?php echo $sale['Pname'] ?></td>
                                                <td><?php echo $sale['piece'] ?></td>
                                                <td><?php echo $sale['points_multiplier'] ?></td>
                                                <td><?php echo ($sale['Ppoint'] * $sale['piece']); ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                    
                                </table>
                            </div>

                            <i class="fa fa-exchange pull-right fa-2x hidden-md hidden-lg hidden-sm" aria-hidden="true"></i>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>
</body>
</html>