<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<body>

<?php include "inc/header.php"; ?>
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg" >
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">×</button>
</div>
<div class="modal-body">
    <div class="img-abs">
        <img src="<?php echo base_url();?>assets/images/50puan.png" width="50%">
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <iframe width="100%" height="400" id="iframeSRC" src="https://www.youtube.com/embed/C9IfDkLLXYE" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</div>
</div>
</div>
</div>



<div class="content" id="f-egitim">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="page-title">EĞİTİM</div>
                </div>
            </div>
            <div class="row videoListLink">

                <?php foreach ($videos as $video): ?>
                    <?php if ($video['video'] == null || $video['video'] == ""): ?>

                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <a href="<?php echo $video['link']; ?>" id="video<?php echo $video['id']; ?>" partner_id="<?php echo $_SESSION['partner_id']; ?>" target="_blank" class="egitim-box" style="background-image: url('<?php echo base_url();?>uploaded_files/<?php echo $video['img']; ?>');">
                            <span><?php echo $video['title']; ?></span>
                        </a>

                    </div>

                    <?php endif ?>
                <?php endforeach ?>

            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>

<script type="text/javascript">
    $('.close, #myModal').click(function(){
        
        var path = 'https://www.youtube.com/embed/XSLPOdrZXvs?autoplay=0';
        $('#iframeSRC').attr('src',path);
        
        clearInterval(countdown);

    });
</script>

</body>
</html>