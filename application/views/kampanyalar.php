﻿<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>
<body>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="page-title">KAMPANYALAR</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="akordion-box">
                        
                        <a class="akordion-img" data-toggle="collapse" data-target="#kampanya4">
                            <img src="<?php echo base_url();?>assets/images/kampanya-vespa.png" class="img-responsive" >
                        </a>
                        <div class="akordion-text collapse" id="kampanya4">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <b>Storage satışlarından 6000 puanı toplayın, kırmızı Vespa’yı kapın!</b><br>
                                <br>
                                Efsanevi Vespa 946’nın eşsiz şıklığını geleceğin tasarımıyla birleştiren Vespa Primavera’ya ulaşmak artık elinizde. İlk bakışta göze çarpan yeni gövde tasarımı, yeni eklenen özellikleri ve daha da yükselen kalitesiyle öne çıkan Vespa Primavera’yı kazanmak için tek yapmanız gereken, Mart 2017’ye kadar 6000 puan değerinde Eternus ürünü satışı gerçekleştirmek. Çekiliş yok, kura yok…<br>
                                <br>
                                <b>* Minimum 75.000$'lık Eternus satışı için geçerlidir.</b>
                            </div>
                            
                        </div>

                    </div>

                    <div class="akordion-box">
                        
                        <a class="akordion-img" data-toggle="collapse" data-target="#kampanya5">
                            <img src="<?php echo base_url();?>assets/images/kampanya-etstur.png" class="img-responsive">
                        </a>
                        <div class="akordion-text collapse" id="kampanya5">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <b>2000 puanı toplayın, tatile ETS Tur rahatlığıyla gidin</b><br>
                                <br>
                                Tatil planlarınızı şimdiden yapmaya başlayın. Tüm ürün gruplarında yapacağınız satışlardan toplayacağınız 2000 puanla ETS Tur’dan 500 TL değerinde hediye çeki kazanabilirsiniz. Herhangi bir ürün sınırlaması, katılım dönemi yok. Yapmanız gereken tek şey 2000 puanı toplamak ve sonrasında tatil için bavullarınızı toplamak…
                            </div>
                            
                        </div>

                    </div>
<!--
                    <div class="akordion-box">
                        
                        <a class="akordion-img" data-toggle="collapse" data-target="#kampanya1">
                            <img src="<?php echo base_url();?>uploaded_files/kampanya1.png" class="img-responsive">
                        </a>
                        <div class="akordion-text collapse" id="kampanya1">

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <b>RX2540 M2 Server Konfigürasyonu</b><br>
                                <br>
                                • 1x Intel Xeon E5-2620v4 8C/16T 2.10 GHz <br>
                                • 32 GB Ram<br>
                                • 2 x HD SAS 12G 600GB 10K 512n HOT PL 2.5'<br>
                                • SFF8644 PSAS CP400e LP<br>
                                • 4 x 1Gb T OCl14000-LOM Arayüzü<br>
                                • 2 x PSU 800W Platinum hp
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <b>DX100 S3 Storage Konfigürasyonu</b><br>
                                <br>
                                • DX100 S3 Cache 8GB <br>
                                • 6x HDD SAS 1.2TB 10k 2.5 inç<br>
                                • RAW Capacity = 6,40 TB<br>
                                • 4 x Channel 6 Gbit/s SAS Port
                            </div>

                            <div class="kampanya-foot">
                                <span>Fujitsu, VMware performans değerlendirmesine göre lider durumdadır.</span><br>
                                http://www.vmware.com/products/vmmark/results.html<br>
                                <br>
                                *Kurulum dahil değildir, isteğe göre ayrıca fiyatlandırılacaktır.<br>
                                *Kampanya bitiş tarihi: 30.11.2016<br>
                                *Fiyatlara KDV dahil değildir.
                            </div>
                            
                        </div>

                    </div>

                    <div class="akordion-box">
                        
                        <a class="akordion-img" data-toggle="collapse" data-target="#kampanya2">
                            <img src="<?php echo base_url();?>assets/images/kampanya2.png" class="img-responsive">
                        </a>
                        <div class="akordion-text collapse" id="kampanya2">

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <b>RX2510 M2 Server Konfigürasyonu</b><br>
                                <br>
                                • 2 x Intel Xeon E5-2620v4 8C/16T 2.10 GHz<br>
                                • 128 GB Ram<br>
                                • 2 x 1 Channel 8Gbit/s FC Controller<br>
                                • 2x1Gbit Cu Intel I350-T2 LP<br>
                                • 2 x  PSU 450W Platinum hp<br>
                                • VMware vSphere Essential Plus Kit (1 yıl servis desteği)<br>
                                • 3 Yıl Yerinde Servis, 9x5, Ertesi İş Günü Müdahale
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <b>DX100 S3 Storage Konfigürasyonu</b><br>
                                <br>
                                • DX100 S3 Cache 8GB<br>
                                • 12 x HDD SAS 1.2TB 10k 2.5 inç<br>
                                • RAW Capacity = 12,80TB.<br>
                                • 8 x Channel 8Gbit/s FC Port
                            </div>

                            <div class="kampanya-foot">
                                <span>Fujitsu, VMware performans değerlendirmesine göre lider durumdadır.</span><br>
                                http://www.vmware.com/products/vmmark/results.html<br>
                                <br>
                                *Kurulum dahil değildir, isteğe göre ayrıca fiyatlandırılacaktır.<br>
                                *Kampanya bitiş tarihi: 30.11.2016<br>
                                *Fiyatlara KDV dahil değildir.
                            </div>
                            
                        </div>

                    </div>

                    <div class="akordion-box">
                        
                        <a class="akordion-img" data-toggle="collapse" data-target="#kampanya3">
                            <img src="<?php echo base_url();?>assets/images/kampanya3.png" class="img-responsive">
                        </a>
                        <div class="akordion-text collapse" id="kampanya3">

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <b>RX2540 M2 Server Konfigürasyonu</b><br>
                                <br>
                                • 2 x Intel Xeon E5-2640v4 10C/20T 2.40 GHz<br>
                                • 128 GB Ram<br>
                                • 2 x 1 Channel 16Gbit/s FC Controller<br>
                                • 4 x 1Gb T OCl14000-LOM Interface<br>
                                • 2 x PSU 800W Platinum hp<br>
                                • VMware vSphere Essential Plus Kit (1 yıl servis desteği)<br>
                                • 5 Yıl Yerinde Servis, 9x5, Ertesi İş Günü Müdahale
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <b>DX200S3 Storage Configuration</b><br>
                                <br>
                                • DX200 S3 Cache 16GB<br>
                                • 24 x HDD SAS 1.2 TB 10k 2.5 inç<br>
                                • RAW Capacity = 25,61 TB.<br>
                                • 8 x Channel 16Gbit/s FC Port
                            </div>

                            <div class="kampanya-foot">
                                <span>Fujitsu, VMware performans değerlendirmesine göre lider durumdadır.</span><br>
                                http://www.vmware.com/products/vmmark/results.html<br>
                                <br>
                                *Kurulum dahil değildir, isteğe göre ayrıca fiyatlandırılacaktır.<br>
                                *Kampanya bitiş tarihi: 30.11.2016<br>
                                *Fiyatlara KDV dahil değildir.
                            </div>
                            
                        </div>

-->

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>
</body>
</html>