<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<?php include "inc/head.php"; ?>

<script type="text/javascript">
    $(document).ready(function(){
        //ProgressBar start
        var $ppc = $('.progress-pie-chart');
        percent = parseInt($ppc.data('percent'));
        deg = 360*percent/100;
        if (percent > 50) {
            $ppc.addClass('gt-50');
        }
        $('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
        $('.ppc-percents span').html(percent+'%');

        var $ppc1 = $('.progress-pie-chart1');
        percent1 = parseInt($ppc1.data('percent1'));
        deg1 = 360*percent1/100;
        if (percent1 > 50) {
            $ppc1.addClass('gt-501');
        }
        $('.ppc-progress-fill1').css('transform','rotate('+ deg1 +'deg)');
        $('.ppc-percents1 span').html(percent1+'%');
        //ProgressBar finish
    });
</script>

<body>

<div class="hediye-popup-bg"></div>
<div class="container hediye-container">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-1"></div>
        <div class="col-md-6 col-sm-6 col-xs-10">
            <div class="hediye-popup">
                <div class="row">
                    <div class="col-md-12">
                        <div class="head">
                            <span class="hediye-close">X</span>
                        </div>
                    </div>
                    <div class="succes-text"></div>
                </div>
            </div>
            <form name="hediyeForm" method="POST" action="<?php echo base_url(); ?>hediyeler/add">
                <input type="hidden" name="hediye" value="14" id="gift_value">
                <input type="hidden" name="profil" value="profil" id="gift_value">
                <input type="submit" id="gift_add" style="display: none;">
            </form>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-1"></div>
    </div>
</div>

<?php include "inc/header.php"; ?>
<div class="content">
    <div class="profil-cont">
        <div class="container">
            <div class="row">
                <?php include 'inc/profil-left.php'; ?>

                <div class="col-md-9 col-sm-12 col-xs-12">
                    
                    <div class="profil-right">

                        <?php include 'inc/profil-top.php'; ?>
                        
                        <div class="profil-graph">
                            
                            <div class="graphBoxes-title">SATIŞLARIM</div>

                            <div class="row" style="display:none">
                                <div class="profil-graphBoxes">

                                    <div class="col-md-5 col-sm-6 col-xs-12">

                                        <div class="graphBox-title">Vespa (Puan Hedefi)</div>

                                        <div class="profil-graphBox">

                                            <div class="graphBox-left">

                                                <div class="progress-pie-chart" data-percent="<?php echo $eternus_toplam / 60; ?>">
                                                  <div class="ppc-progress">
                                                    <div class="ppc-progress-fill"></div>
                                                  </div>
                                                  <div class="ppc-percents">
                                                    <div class="pcc-percents-wrapper">
                                                      <span>%</span>
                                                    </div>
                                                  </div>
                                                </div>

                                            </div>

                                            <div class="graphBox-right">

                                                <b>Hedefim</b>
                                                <br>
                                                6000
                                                <br><br>
                                                <b>Gerçekleşen</b>
                                                <br>
                                                <red><?php echo $eternus_toplam; ?></red>
                                                
                                            </div>
                                            
                                        </div>

                                    </div>

                                    <div class="col-md-1"></div>

                                    <div class="col-md-5 col-sm-6 col-xs-12 pull-right">

                                        <div class="graphBox-title">Vespa (Para Hedefi)</div>

                                        <div class="profil-graphBox">

                                            <div class="graphBox-left">

                                            <div class="progress-pie-chart1" data-percent1="<?php echo ($eternus_toplam * 12.5) / 750; ?>">
                                                  <div class="ppc-progress1">
                                                    <div class="ppc-progress-fill1"></div>
                                                  </div>
                                                  <div class="ppc-percents1 ">
                                                    <div class="pcc-percents-wrapper1">
                                                      <span>%</span>
                                                    </div>
                                                  </div>
                                                </div>

                                            </div>

                                            <div class="graphBox-right">

                                                <b>Hedefim</b>
                                                <br>
                                                75.000$
                                                <br><br>
                                                <b>Gerçekleşen</b>
                                                <br>
                                                <red><?php echo $eternus_toplam * 12.5; ?>$</red>
                                                
                                            </div>
                                            
                                        </div>

                                        <div class="pull-right graphBox-note-area">
                                            <div class="graphBox-note">*Min. 75.000$’lık Eternus alımlarında geçerlidir.</div>
                                        </div>

                                    </div>

                                    <div class="col-md-2"></div>
                                    
                                </div>
                            </div>

                        </div>

                        <?php  if ($eternus_toplam >= 6000) { ?> 
                            <div class="row" style="display:none;">
                                <div class="col-md-12">
                                    <div onclick="giftSend()" style="cursor: pointer;">
                                        <img src="<?php echo base_url();?>assets/images/vespa-hediye.png" class="img-responsive"  style="margin-top: -20px;" />
                                    </div>
                                </div>   
                            </div>
                        <?php  } ?>

                        <div class="profil-salesBox">
                            
                            <div class="salesBox-title">SATIŞLARIM</div>

                            <div class="table-responsive border-none" >
                                <table width="100%" border="0" class="salesTable table">
                                    <thead>
                                        <tr>
                                            <th>Fatura Numarası</th>
                                            <th>Fatura Tarihi</th>
                                            <th>Ürün Marka</th>
                                            <th>Ürün Model</th>
                                            <th>Ürün Number</th>
                                            <th>Ürün Adı</th>
                                            <th>Ürün Adeti</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php foreach ($sales as $sale): ?>
                                            <tr>
                                                <td><?php echo $sale['Ino'] ?></td>
                                                <td><?php echo $sale['Icreated'] ?></td>
                                                <td><?php echo $sale['brand'] ?></td>
                                                <td><?php echo $sale['model'] ?></td>
                                                <td><?php echo $sale['Pno'] ?></td>
                                                <td><?php echo $sale['Pname'] ?></td>
                                                <td><?php echo $sale['piece'] ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                   
                                </table>
                            </div>
                            <i class="fa fa-exchange pull-right fa-2x hidden-lg hidden-md hidden-sm" aria-hidden="true"></i>

                        </div>

                        <div class="pull-right graphBox-note-area">
                            <div class="graphBox-note">*Bir Primergy satışı: 480 puan</div>
                            <div class="graphBox-note">*Bir Eternus satışı: 1200 puan</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<canvas id="canvas"></canvas>

<script type="text/javascript">
    function giftSend(){
        $('#gift_add').click();
    }


// confetti script (start)

    // globals
    var canvas;
    var ctx;
    var W;
    var H;
    var mp = 150; //max particles
    var particles = [];
    var angle = 0;
    var tiltAngle = 0;
    var confettiActive = true;
    var animationComplete = true;
    var deactivationTimerHandler;
    var reactivationTimerHandler;
    var animationHandler;

    // objects

    var particleColors = {
        colorOptions: ["DodgerBlue", "OliveDrab", "Gold", "pink", "SlateBlue", "lightblue", "Violet", "PaleGreen", "SteelBlue", "SandyBrown", "Chocolate", "Crimson"],
        colorIndex: 0,
        colorIncrementer: 0,
        colorThreshold: 10,
        getColor: function () {
            if (this.colorIncrementer >= 10) {
                this.colorIncrementer = 0;
                this.colorIndex++;
                if (this.colorIndex >= this.colorOptions.length) {
                    this.colorIndex = 0;
                }
            }
            this.colorIncrementer++;
            return this.colorOptions[this.colorIndex];
        }
    }

    function confettiParticle(color) {
        this.x = Math.random() * W; // x-coordinate
        this.y = (Math.random() * H) - H; //y-coordinate
        this.r = RandomFromTo(10, 30); //radius;
        this.d = (Math.random() * mp) + 10; //density;
        this.color = color;
        this.tilt = Math.floor(Math.random() * 10) - 10;
        this.tiltAngleIncremental = (Math.random() * 0.07) + .05;
        this.tiltAngle = 0;

        this.draw = function () {
            ctx.beginPath();
            ctx.lineWidth = this.r / 2;
            ctx.strokeStyle = this.color;
            ctx.moveTo(this.x + this.tilt + (this.r / 4), this.y);
            ctx.lineTo(this.x + this.tilt, this.y + this.tilt + (this.r / 4));
            return ctx.stroke();
        }
    }

    $(document).ready(function () {
        SetGlobals();
        InitializeButton();
        // InitializeConfetti();

        $(window).resize(function () {
            W = window.innerWidth;
            H = window.innerHeight;
            canvas.width = W - 20;
            canvas.height = H;
        });

    });

    function InitializeButton() {
        $('#stopButton').click(DeactivateConfetti);
        $('#startButton').click(RestartConfetti);
    }

    function SetGlobals() {
        canvas = document.getElementById("canvas");
        ctx = canvas.getContext("2d");
        W = window.innerWidth;
        H = window.innerHeight;
        canvas.width = W - 20;
        canvas.height = H;
    }

    function InitializeConfetti() {
        particles = [];
        animationComplete = false;
        for (var i = 0; i < mp; i++) {
            var particleColor = particleColors.getColor();
            particles.push(new confettiParticle(particleColor));
        }
        StartConfetti();
    }

    function Draw() {
        ctx.clearRect(0, 0, W, H);
        var results = [];
        for (var i = 0; i < mp; i++) {
            (function (j) {
                results.push(particles[j].draw());
            })(i);
        }
        Update();

        return results;
    }

    function RandomFromTo(from, to) {
        return Math.floor(Math.random() * (to - from + 1) + from);
    }


    function Update() {
        var remainingFlakes = 0;
        var particle;
        angle += 0.01;
        tiltAngle += 0.1;

        for (var i = 0; i < mp; i++) {
            particle = particles[i];
            if (animationComplete) return;

            if (!confettiActive && particle.y < -15) {
                particle.y = H + 100;
                continue;
            }

            stepParticle(particle, i);

            if (particle.y <= H) {
                remainingFlakes++;
            }
            CheckForReposition(particle, i);
        }

        if (remainingFlakes === 0) {
            StopConfetti();
        }
    }

    function CheckForReposition(particle, index) {
        if ((particle.x > W + 20 || particle.x < -20 || particle.y > H) && confettiActive) {
            if (index % 5 > 0 || index % 2 == 0) //66.67% of the flakes
            {
                repositionParticle(particle, Math.random() * W, -10, Math.floor(Math.random() * 10) - 10);
            } else {
                if (Math.sin(angle) > 0) {
                    //Enter from the left
                    repositionParticle(particle, -5, Math.random() * H, Math.floor(Math.random() * 10) - 10);
                } else {
                    //Enter from the right
                    repositionParticle(particle, W + 5, Math.random() * H, Math.floor(Math.random() * 10) - 10);
                }
            }
        }
    }
    function stepParticle(particle, particleIndex) {
        particle.tiltAngle += particle.tiltAngleIncremental;
        particle.y += (Math.cos(angle + particle.d) + 3 + particle.r / 2) / 2;
        particle.x += Math.sin(angle);
        particle.tilt = (Math.sin(particle.tiltAngle - (particleIndex / 3))) * 15;
    }

    function repositionParticle(particle, xCoordinate, yCoordinate, tilt) {
        particle.x = xCoordinate;
        particle.y = yCoordinate;
        particle.tilt = tilt;
    }

    function StartConfetti() {
        W = window.innerWidth;
        H = window.innerHeight;
        canvas.width = W - 20;
        canvas.height = H;
        (function animloop() {
            if (animationComplete) return null;
            animationHandler = requestAnimFrame(animloop);
            return Draw();
        })();
    }

    function ClearTimers() {
        clearTimeout(reactivationTimerHandler);
        clearTimeout(animationHandler);
    }

    function DeactivateConfetti() {
        confettiActive = false;
        ClearTimers();
    }

    function StopConfetti() {
        animationComplete = true;
        if (ctx == undefined) return;
        ctx.clearRect(0, 0, W, H);
    }

    function RestartConfetti() {
        ClearTimers();
        StopConfetti();
        reactivationTimerHandler = setTimeout(function () {
            confettiActive = true;
            animationComplete = false;
            InitializeConfetti();
        }, 100);

    }

    window.requestAnimFrame = (function () {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
            return window.setTimeout(callback, 1000 / 60);
        };
    })();


// confetti script (finish)

</script>

<?php include "inc/footer.php"; ?>
<?php include "inc/script.php"; ?>
</body>
</html>